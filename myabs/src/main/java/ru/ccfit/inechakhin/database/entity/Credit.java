package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "credits")
public class Credit {

    public Credit () {
    }

    public Credit (Long id, BigDecimal remainingAmount, BigDecimal remainingTerm, BigDecimal annualRate,
                   LocalDateTime updatedAt, LocalDateTime createdAt, CreditStatus status, Account account) {
        this.id = id;
        this.remainingAmount = remainingAmount;
        this.remainingTerm = remainingTerm;
        this.annualRate = annualRate;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.status = status;
        this.account = account;
    }

    public Credit (Long id, BigDecimal remainingAmount, BigDecimal remainingTerm, BigDecimal annualRate, CreditStatus status, Account account) {
        this.id = id;
        this.remainingAmount = remainingAmount;
        this.remainingTerm = remainingTerm;
        this.annualRate = annualRate;
        this.status = status;
        this.account = account;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "remaining_amount", columnDefinition = "numeric(100, 2) default 0.00", nullable = false)
    private BigDecimal remainingAmount = BigDecimal.ZERO;

    @Column(name = "remaining_term", columnDefinition = "numeric(7, 2) default 0.00", nullable = false)
    private BigDecimal remainingTerm = BigDecimal.ZERO;

    @Column(name = "annual_rate", columnDefinition = "numeric(4, 2) default 0.00", nullable = false)
    private BigDecimal annualRate = BigDecimal.ZERO;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "credit_statuses_id", nullable = false)
    private CreditStatus status;

    @OneToOne
    @JoinColumn(name = "accounts_id")
    private Account account;

    public void subtractRemainingAmount (BigDecimal amount) {
        this.remainingAmount = this.remainingAmount.subtract(amount);
    }

    public void subtractRemainingTerm (BigDecimal term) {
        this.remainingTerm = this.remainingTerm.subtract(term);
    }

    public Long getId () {
        return id;
    }

    public BigDecimal getRemainingAmount () {
        return remainingAmount;
    }

    public BigDecimal getRemainingTerm () {
        return remainingTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public CreditStatus getStatus () {
        return status;
    }

    public Account getAccount () {
        return account;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setRemainingAmount (BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public void setRemainingTerm (BigDecimal remainingTerm) {
        this.remainingTerm = remainingTerm;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    public void setStatus (CreditStatus status) {
        this.status = status;
    }

    public void setAccount (Account account) {
        this.account = account;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return Objects.equals(id, credit.id)
                && Objects.equals(remainingAmount, credit.remainingAmount)
                && Objects.equals(remainingTerm, credit.remainingTerm)
                && Objects.equals(annualRate, credit.annualRate);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, remainingAmount, remainingTerm, annualRate);
    }

    @Override
    public String toString () {
        return "Credit{" +
                "id=" + id +
                ", remainingAmount=" + remainingAmount +
                ", remainingTerm=" + remainingTerm +
                ", annualRate=" + annualRate +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
