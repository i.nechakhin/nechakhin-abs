package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "cards")
public class Card {

    public Card () {
    }

    public Card (Long id, Long number, LocalDate expDate, Integer cvv, Account account, CardStatus status, CardPaymentSystem paymentSystem) {
        this.id = id;
        this.number = number;
        this.expDate = expDate;
        this.cvv = cvv;
        this.account = account;
        this.status = status;
        this.paymentSystem = paymentSystem;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "number", nullable = false, unique = true)
    private Long number;

    @Column(name = "exp_date", nullable = false)
    private LocalDate expDate;

    @Column(name = "cvv", nullable = false)
    private Integer cvv;

    @ManyToOne
    @JoinColumn(name = "accounts_id")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "card_statuses_id", nullable = false)
    private CardStatus status;

    @ManyToOne
    @JoinColumn(name = "payment_system_id", nullable = false)
    private CardPaymentSystem paymentSystem;

    public Long getId () {
        return id;
    }

    public Long getNumber () {
        return number;
    }

    public LocalDate getExpDate () {
        return expDate;
    }

    public Integer getCvv () {
        return cvv;
    }

    public Account getAccount () {
        return account;
    }

    public CardStatus getStatus () {
        return status;
    }

    public CardPaymentSystem getPaymentSystem () {
        return paymentSystem;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setNumber (Long number) {
        this.number = number;
    }

    public void setExpDate (LocalDate expDate) {
        this.expDate = expDate;
    }

    public void setCvv (Integer cvv) {
        this.cvv = cvv;
    }

    public void setAccount (Account account) {
        this.account = account;
    }

    public void setStatus (CardStatus status) {
        this.status = status;
    }

    public void setPaymentSystem (CardPaymentSystem paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(id, card.id)
                && Objects.equals(number, card.number)
                && Objects.equals(expDate, card.expDate)
                && Objects.equals(cvv, card.cvv);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, number, expDate, cvv);
    }

    @Override
    public String toString () {
        return "Card{" +
                "id=" + id +
                ", number=" + number +
                ", expDate=" + expDate +
                ", cvv=" + cvv +
                '}';
    }
}
