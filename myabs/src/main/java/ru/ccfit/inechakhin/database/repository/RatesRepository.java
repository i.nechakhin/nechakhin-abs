package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.Rates;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface RatesRepository extends JpaRepository<Rates, Long> {
    Optional<Rates> findByMinAmountAndMaxAmountAndMinTermAndMaxTermAndAnnualRate(BigDecimal minAmount, BigDecimal maxAmount, BigDecimal minTerm, BigDecimal maxTerm, BigDecimal annualRate);
}
