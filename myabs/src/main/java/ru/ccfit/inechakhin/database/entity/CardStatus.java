package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "card_statuses")
public class CardStatus {

    public CardStatus () {
    }

    public CardStatus (Long id, String name, List<Card> cards) {
        this.id = id;
        this.name = name;
        this.cards = cards;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "status")
    private List<Card> cards;

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<Card> getCards () {
        return cards;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setCards (List<Card> cards) {
        this.cards = cards;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardStatus that = (CardStatus) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name);
    }

    @Override
    public String toString () {
        return "CardStatus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        ACTIVE,
        BLOCKED,
        DEACTIVATED
    }
}
