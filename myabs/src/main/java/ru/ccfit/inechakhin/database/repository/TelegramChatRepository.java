package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.TelegramChat;

import java.util.Optional;

@Repository
public interface TelegramChatRepository extends JpaRepository<TelegramChat, Long> {
    Optional<TelegramChat> findByToken (String token);

    Optional<TelegramChat> findByUserId (Long userId);
}
