package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.TransactionSource;

import java.util.Optional;

@Repository
public interface TransactionSourceRepository extends JpaRepository<TransactionSource, Long> {
    Optional<TransactionSource> findByName (String name);
}
