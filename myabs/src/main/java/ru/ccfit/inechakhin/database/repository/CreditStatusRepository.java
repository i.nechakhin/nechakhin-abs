package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.CreditStatus;

@Repository
public interface CreditStatusRepository extends JpaRepository<CreditStatus, Long> {
    CreditStatus findByName (String name);
}
