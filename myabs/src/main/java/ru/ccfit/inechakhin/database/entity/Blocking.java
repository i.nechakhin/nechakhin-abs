package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "blocking")
public class Blocking {

    public Blocking () {
    }

    public Blocking (Long id, LocalDateTime startBlock, LocalDateTime endBlock, Account account) {
        this.id = id;
        this.startBlock = startBlock;
        this.endBlock = endBlock;
        this.account = account;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "start_block", nullable = false)
    private LocalDateTime startBlock;

    @Column(name = "end_block", nullable = false)
    private LocalDateTime endBlock;

    @ManyToOne
    @JoinColumn(name = "accounts_id")
    private Account account;

    public Long getId () {
        return id;
    }

    public LocalDateTime getStartBlock () {
        return startBlock;
    }

    public LocalDateTime getEndBlock () {
        return endBlock;
    }

    public Account getAccount () {
        return account;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setStartBlock (LocalDateTime startBlock) {
        this.startBlock = startBlock;
    }

    public void setEndBlock (LocalDateTime endBlock) {
        this.endBlock = endBlock;
    }

    public void setAccount (Account account) {
        this.account = account;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Blocking blocking = (Blocking) o;
        return Objects.equals(id, blocking.id)
                && Objects.equals(startBlock, blocking.startBlock)
                && Objects.equals(endBlock, blocking.endBlock);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, startBlock, endBlock);
    }

    @Override
    public String toString () {
        return "Card{" +
                "id=" + id +
                ", startBlock=" + startBlock +
                ", endBlock=" + endBlock +
                '}';
    }
}
