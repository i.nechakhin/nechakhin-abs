package ru.ccfit.inechakhin.database.entity;

import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "transactions")
public class Transaction {

    public Transaction () {
    }

    public Transaction (Long id, BigDecimal amount, Integer verificationCode, LocalDateTime createdAt,
                        LocalDateTime updatedAt, Account accountFrom, Account accountTo, TransactionSource sourceFrom,
                        TransactionSource sourceTo, TransactionStatus status, TransactionType type) {
        this.id = id;
        this.amount = amount;
        this.verificationCode = verificationCode;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.sourceFrom = sourceFrom;
        this.sourceTo = sourceTo;
        this.status = status;
        this.type = type;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "verification_code")
    private Integer verificationCode;

    @CreationTimestamp
    @Column(name = "created_at", columnDefinition = "timestamp default now()", nullable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", columnDefinition = "timestamp default now()", nullable = false)
    private LocalDateTime updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id_from", nullable = false)
    private Account accountFrom;

    @ManyToOne
    @JoinColumn(name = "account_id_to", nullable = false)
    private Account accountTo;

    @ManyToOne
    @JoinColumn(name = "source_id_from", nullable = false)
    private TransactionSource sourceFrom;

    @ManyToOne
    @JoinColumn(name = "source_id_to", nullable = false)
    private TransactionSource sourceTo;

    @ManyToOne
    @JoinColumn(name = "transaction_statuses_id", nullable = false)
    private TransactionStatus status;

    @ManyToOne
    @JoinColumn(name = "transaction_types_id", nullable = false)
    private TransactionType type;

    public Long getId () {
        return id;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public Integer getVerificationCode () {
        return verificationCode;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public Account getAccountFrom () {
        return accountFrom;
    }

    public Account getAccountTo () {
        return accountTo;
    }

    public TransactionStatus getStatus () {
        return status;
    }

    public TransactionType getType () {
        return type;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public void setVerificationCode (Integer verificationCode) {
        this.verificationCode = verificationCode;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setAccountFrom (Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public void setAccountTo (Account accountTo) {
        this.accountTo = accountTo;
    }

    public TransactionSource getSourceFrom () {
        return sourceFrom;
    }

    public void setSourceFrom (TransactionSource sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

    public TransactionSource getSourceTo () {
        return sourceTo;
    }

    public void setSourceTo (TransactionSource sourceTo) {
        this.sourceTo = sourceTo;
    }

    public void setStatus (TransactionStatus status) {
        this.status = status;
    }

    public void setType (TransactionType type) {
        this.type = type;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id)
                && Objects.equals(amount, that.amount)
                && Objects.equals(verificationCode, that.verificationCode)
                && Objects.equals(createdAt, that.createdAt)
                && Objects.equals(updatedAt, that.updatedAt)
                && Objects.equals(accountFrom, that.accountFrom)
                && Objects.equals(accountTo, that.accountTo)
                && Objects.equals(sourceFrom, that.sourceFrom)
                && Objects.equals(sourceTo, that.sourceTo)
                && Objects.equals(status, that.status)
                && Objects.equals(type, that.type);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, amount, verificationCode,
                createdAt, updatedAt,
                accountFrom, accountTo,
                sourceFrom, sourceTo,
                status, type);
    }

    @Override
    public String toString () {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", verificationCode=" + verificationCode +
                ", createdAt=" + createdAt +
                ", updated_at=" + updatedAt +
                '}';
    }
}
