package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "transaction_statuses")
public class TransactionStatus {

    public TransactionStatus () {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "status")
    private List<Transaction> transactions;

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<Transaction> getTransactions () {
        return transactions;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setTransactions (List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionStatus transactionStatus = (TransactionStatus) o;
        return Objects.equals(id, transactionStatus.id)
                && Objects.equals(name, transactionStatus.name)
                && Objects.equals(transactions, transactionStatus.transactions);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name, transactions);
    }

    @Override
    public String toString () {
        return "Status{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        IN_PROCESS,
        SUCCESS,
        DECLINE
    }
}
