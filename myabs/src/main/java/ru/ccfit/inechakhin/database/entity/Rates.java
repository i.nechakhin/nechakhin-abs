package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.*;

@Entity
@Table(name = "rates")
public class Rates {

    public Rates () {
    }

    public Rates (Long id, BigDecimal minAmount, BigDecimal maxAmount, BigDecimal minTerm, BigDecimal maxTerm, BigDecimal annualRate, List<Account> transactionsTo) {
        this.id = id;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.minTerm = minTerm;
        this.maxTerm = maxTerm;
        this.annualRate = annualRate;
        this.transactionsTo = transactionsTo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "min_amount", columnDefinition = "numeric(100, 2) default 0.00", nullable = false)
    private BigDecimal minAmount = BigDecimal.ZERO;

    @Column(name = "max_amount", columnDefinition = "numeric(100, 2) default 0.00", nullable = false)
    private BigDecimal maxAmount = BigDecimal.ZERO;

    @Column(name = "min_term", columnDefinition = "numeric(7, 2) default 0.00", nullable = false)
    private BigDecimal minTerm = BigDecimal.ZERO;

    @Column(name = "max_term", columnDefinition = "numeric(7, 2) default 0.00", nullable = false)
    private BigDecimal maxTerm = BigDecimal.ZERO;

    @Column(name = "annual_rate", columnDefinition = "numeric(5, 2) default 0.00", nullable = false)
    private BigDecimal annualRate = BigDecimal.ZERO;

    @OneToMany(mappedBy = "rates")
    private List<Account> transactionsTo;

    public Long getId () {
        return id;
    }

    public BigDecimal getMinAmount () {
        return minAmount;
    }

    public BigDecimal getMaxAmount () {
        return maxAmount;
    }

    public BigDecimal getMinTerm () {
        return minTerm;
    }

    public BigDecimal getMaxTerm () {
        return maxTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public List<Account> getTransactionsTo () {
        return transactionsTo;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setMinAmount (BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public void setMaxAmount (BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void setMinTerm (BigDecimal minTerm) {
        this.minTerm = minTerm;
    }

    public void setMaxTerm (BigDecimal maxTerm) {
        this.maxTerm = maxTerm;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    public void setTransactionsTo (List<Account> transactionsTo) {
        this.transactionsTo = transactionsTo;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rates rates = (Rates) o;
        return Objects.equals(id, rates.id)
                && Objects.equals(minAmount, rates.minAmount)
                && Objects.equals(maxAmount, rates.maxAmount)
                && Objects.equals(minTerm, rates.minTerm)
                && Objects.equals(maxTerm, rates.maxTerm)
                && Objects.equals(annualRate, rates.annualRate);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, minAmount, maxAmount, minTerm, maxTerm, annualRate);
    }

    @Override
    public String toString () {
        return "Card{" +
                "id=" + id +
                ", minAmount=" + minAmount +
                ", maxAmount=" + maxAmount +
                ", minTerm=" + minTerm +
                ", maxTerm=" + maxTerm +
                ", annualRate=" + annualRate +
                '}';
    }
}
