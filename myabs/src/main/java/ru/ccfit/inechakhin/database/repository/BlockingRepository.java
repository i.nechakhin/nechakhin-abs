package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.Blocking;

import java.util.List;

@Repository
public interface BlockingRepository extends JpaRepository<Blocking, Long> {
    List<Blocking> findAllByAccountId (Long accountId);
}
