package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.CardPaymentSystem;

@Repository
public interface CardPaymentSystemRepository extends JpaRepository<CardPaymentSystem, Long> {
    CardPaymentSystem findByName (String name);
}