package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "telegram_chats")
public class TelegramChat {

    public TelegramChat () {
    }

    public TelegramChat (Long id, Long chatId, String token, LocalDate expTokenDate, User user) {
        this.id = id;
        this.chatId = chatId;
        this.token = token;
        this.expTokenDate = expTokenDate;
        this.user = user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "token")
    private String token;

    @Column(name = "exp_token_date")
    private LocalDate expTokenDate;

    @OneToOne
    @JoinColumn(name = "users_id")
    private User user;

    public Long getId () {
        return id;
    }

    public Long getChatId () {
        return chatId;
    }

    public String getToken () {
        return token;
    }

    public LocalDate getExpTokenDate () {
        return expTokenDate;
    }

    public User getUser () {
        return user;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setChatId (Long chatId) {
        this.chatId = chatId;
    }

    public void setToken (String token) {
        this.token = token;
    }

    public void setExpTokenDate (LocalDate expTokenDate) {
        this.expTokenDate = expTokenDate;
    }

    public void setUser (User user) {
        this.user = user;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelegramChat that = (TelegramChat) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id);
    }
}
