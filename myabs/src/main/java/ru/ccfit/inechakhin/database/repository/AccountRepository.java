package ru.ccfit.inechakhin.database.repository;

import ru.ccfit.inechakhin.database.entity.Account;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.*;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> findAllByUserId (Long userId);

    List<Account> findAccountsByUserIdAndStatusId (Long userId, Long statusId);
}
