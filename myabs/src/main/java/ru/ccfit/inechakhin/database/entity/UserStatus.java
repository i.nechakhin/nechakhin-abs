package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "user_statuses")
public class UserStatus {

    public UserStatus () {
    }

    public UserStatus (Long id, String name, List<User> users) {
        this.id = id;
        this.name = name;
        this.users = users;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "status")
    private List<User> users;

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<User> getUsers () {
        return users;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setUsers (List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserStatus that = (UserStatus) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name);
    }

    @Override
    public String toString () {
        return "UserStatus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        ACTIVE,
        BLOCKED,
        DELETED
    }
}
