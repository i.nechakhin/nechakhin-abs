package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "account_statuses")
public class AccountStatus {

    public AccountStatus () {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "status")
    private List<Account> accounts;

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<Account> getAccounts () {
        return accounts;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setAccounts (List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountStatus that = (AccountStatus) o;
        if (!id.equals(that.id)) return false;
        return name.equals(that.name);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name);
    }

    @Override
    public String toString () {
        return "AccountStatus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        CREATED,
        ACTIVE,
        BLOCKED,
        DELETED
    }
}
