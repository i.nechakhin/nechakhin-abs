package ru.ccfit.inechakhin.database.entity;

import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "roles")
public class Role implements GrantedAuthority {

    public Role () {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    @Override
    public String getAuthority () {
        return getName();
    }

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<User> getUsers () {
        return users;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setUsers (List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(id, role.id)
                && Objects.equals(name, role.name)
                && Objects.equals(users, role.users);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name, users);
    }

    @Override
    public String toString () {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        USER,
        MANAGER
    }
}
