package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "transaction_types")
public class TransactionType {

    public TransactionType () {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "type")
    private List<Transaction> transactions;

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<Transaction> getTransactions () {
        return transactions;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setTransactions (List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionType type = (TransactionType) o;
        return Objects.equals(id, type.id)
                && Objects.equals(name, type.name)
                && Objects.equals(transactions, type.transactions);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name, transactions);
    }

    @Override
    public String toString () {
        return "Type{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        TRANSFER,
        SALARY,
        ORDER,
        CREDIT
    }
}
