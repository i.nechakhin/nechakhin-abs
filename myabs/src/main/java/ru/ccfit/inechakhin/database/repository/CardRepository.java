package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.Card;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    Card findCardByNumber (Long number);

    List<Card> findAllByAccountId (Long accountId);
}