package ru.ccfit.inechakhin.database.entity;

import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.*;

import java.time.*;
import java.util.*;

@Entity
@Table(name = "users")
public class User {

    public User () {
    }

    public User (Long id, String firstName, String lastName, LocalDate birthDate, String email, String phoneNumber,
                 LocalDateTime createdAt, LocalDateTime updatedAt, List<Role> roles, List<Account> accounts,
                 UserStatus status, TelegramChat telegramChat, UserCredential credential) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.roles = roles;
        this.accounts = accounts;
        this.status = status;
        this.telegramChat = telegramChat;
        this.credential = credential;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "phone_number", nullable = false, unique = true)
    private String phoneNumber;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "lnk_users_roles",
            joinColumns = @JoinColumn(name = "users_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "roles_id", referencedColumnName = "id"))
    private List<Role> roles;

    @OneToMany(mappedBy = "user")
    private List<Account> accounts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_statuses_id", nullable = false)
    private UserStatus status;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private TelegramChat telegramChat;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private UserCredential credential;

    public void setCredential (UserCredential credential) {
        this.credential = credential;
        credential.setUser(this);
    }

    public Long getId () {
        return id;
    }

    public String getFirstName () {
        return firstName;
    }

    public String getLastName () {
        return lastName;
    }

    public LocalDate getBirthDate () {
        return birthDate;
    }

    public String getEmail () {
        return email;
    }

    public String getPhoneNumber () {
        return phoneNumber;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public List<Role> getRoles () {
        return roles;
    }

    public List<Account> getAccounts () {
        return accounts;
    }

    public UserStatus getStatus () {
        return status;
    }

    public TelegramChat getTelegramChat () {
        return telegramChat;
    }

    public UserCredential getCredential () {
        return credential;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    public void setBirthDate (LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setRoles (List<Role> roles) {
        this.roles = roles;
    }

    public void setAccounts (List<Account> accounts) {
        this.accounts = accounts;
    }

    public void setStatus (UserStatus status) {
        this.status = status;
    }

    public void setTelegramChat (TelegramChat telegramChat) {
        this.telegramChat = telegramChat;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id)
                && Objects.equals(firstName, user.firstName)
                && Objects.equals(lastName, user.lastName)
                && Objects.equals(birthDate, user.birthDate)
                && Objects.equals(email, user.email)
                && Objects.equals(phoneNumber, user.phoneNumber)
                && Objects.equals(createdAt, user.createdAt)
                && Objects.equals(updatedAt, user.updatedAt);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id,
                firstName, lastName,
                birthDate, email,
                phoneNumber, createdAt,
                updatedAt);
    }

    @Override
    public String toString () {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
