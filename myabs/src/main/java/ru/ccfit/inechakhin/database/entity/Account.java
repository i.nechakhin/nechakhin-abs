package ru.ccfit.inechakhin.database.entity;

import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table(name = "accounts")
public class Account {

    public Account () {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount", columnDefinition = "numeric(100, 2) default 0.00", nullable = false)
    private BigDecimal amount = BigDecimal.ZERO;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "account_statuses_id", nullable = false)
    private AccountStatus status;

    @ManyToOne
    @JoinColumn(name = "rates_id")
    private Rates rates;

    @OneToMany(mappedBy = "account")
    private List<Card> cards;

    @OneToOne(mappedBy = "account")
    private Credit credit;

    @OneToMany(mappedBy = "account")
    private List<Blocking> blockings;

    @OneToMany(mappedBy = "accountFrom")
    private List<Transaction> transactionsFrom;

    @OneToMany(mappedBy = "accountTo")
    private List<Transaction> transactionsTo;

    public void addMoney (BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }

    public void subtractMoney (BigDecimal amount) {
        this.amount = this.amount.subtract(amount);
    }

    public Long getId () {
        return id;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public User getUser () {
        return user;
    }

    public AccountStatus getStatus () {
        return status;
    }

    public Rates getRates () {
        return rates;
    }

    public List<Card> getCards () {
        return cards;
    }

    public Credit getCredit () {
        return credit;
    }

    public List<Blocking> getBlockings () {
        return blockings;
    }

    public List<Transaction> getTransactionsFrom () {
        return transactionsFrom;
    }

    public List<Transaction> getTransactionsTo () {
        return transactionsTo;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUser (User user) {
        this.user = user;
    }

    public void setStatus (AccountStatus status) {
        this.status = status;
    }

    public void setRates (Rates rates) {
        this.rates = rates;
    }

    public void setCards (List<Card> cards) {
        this.cards = cards;
    }

    public void setCredit (Credit credit) {
        this.credit = credit;
    }

    public void setBlockings (List<Blocking> blockings) {
        this.blockings = blockings;
    }

    public void setTransactionsFrom (List<Transaction> transactionsFrom) {
        this.transactionsFrom = transactionsFrom;
    }

    public void setTransactionsTo (List<Transaction> transactionsTo) {
        this.transactionsTo = transactionsTo;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id);
    }

    @Override
    public String toString () {
        return "Account{" +
                "id=" + id +
                ", amount=" + amount +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                ", user=" + user +
                ", status=" + status +
                '}';
    }
}
