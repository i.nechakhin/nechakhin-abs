package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "transaction_source")
public class TransactionSource {

    public TransactionSource () {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "commission", columnDefinition = "numeric(4, 2) default 0.00", nullable = false)
    private BigDecimal commission = BigDecimal.ZERO;

    @OneToMany(mappedBy = "sourceFrom")
    private List<Transaction> transactionsFrom;

    @OneToMany(mappedBy = "sourceTo")
    private List<Transaction> transactionsTo;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public BigDecimal getCommission () {
        return commission;
    }

    public void setCommission (BigDecimal commission) {
        this.commission = commission;
    }

    public List<Transaction> getTransactionsFrom () {
        return transactionsFrom;
    }

    public void setTransactionsFrom (List<Transaction> transactionsFrom) {
        this.transactionsFrom = transactionsFrom;
    }

    public List<Transaction> getTransactionsTo () {
        return transactionsTo;
    }

    public void setTransactionsTo (List<Transaction> transactionsTo) {
        this.transactionsTo = transactionsTo;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionSource transactionStatus = (TransactionSource) o;
        return Objects.equals(id, transactionStatus.id)
                && Objects.equals(name, transactionStatus.name)
                && Objects.equals(commission, transactionStatus.commission)
                && Objects.equals(transactionsFrom, transactionStatus.transactionsFrom)
                && Objects.equals(transactionsTo, transactionStatus.transactionsTo);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name, commission, transactionsFrom, transactionsTo);
    }

    @Override
    public String toString () {
        return "TransactionSource{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", commission=" + commission +
                '}';
    }

    public enum Name {
        BANK_ACCOUNT,
        BANK_ACCOUNT_IN_ANOTHER_BANK,
        YANDEX_MONEY,
        QIWI
    }
}
