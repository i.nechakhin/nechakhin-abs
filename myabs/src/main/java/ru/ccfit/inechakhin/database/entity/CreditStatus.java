package ru.ccfit.inechakhin.database.entity;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "credit_statuses")
public class CreditStatus {

    public CreditStatus () {
    }

    public CreditStatus (Long id, String name, List<Credit> credits) {
        this.id = id;
        this.name = name;
        this.credits = credits;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "status")
    private List<Credit> credits;

    public Long getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public List<Credit> getCredits () {
        return credits;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setCredits (List<Credit> credits) {
        this.credits = credits;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditStatus that = (CreditStatus) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode () {
        return Objects.hash(id, name);
    }

    @Override
    public String toString () {
        return "CardStatus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public enum Name {
        ACTIVE,
        OVERDUE,
        CLOSED
    }
}
