package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Page<Transaction> findAllByAccountFrom_Id (Long accountId, Pageable pageable);

    Page<Transaction> findAllByAccountFrom_IdAndStatus_Id (Long accountId, Long statusId, Pageable pageable);

    Page<Transaction> findAllByAccountFrom_IdAndType_Id (Long accountId, Long typeId, Pageable pageable);

    Page<Transaction> findAllByAccountFrom_IdAndSourceFrom_Id (Long accountId, Long sourceId, Pageable pageable);

    Page<Transaction> findAllByAccountFrom_IdAndSourceTo_Id (Long accountId, Long sourceId, Pageable pageable);
}
