package ru.ccfit.inechakhin.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ccfit.inechakhin.database.entity.AccountStatus;

@Repository
public interface AccountStatusRepository extends JpaRepository<AccountStatus, Long> {
    AccountStatus findByName (String name);
}