package ru.ccfit.inechakhin.exception;

public class UserException extends RuntimeException {
    public UserException (String message) {
        super(message);
    }

    public static UserException userIsBlocked (String identification) {
        return new UserException(String.format("User %s is blocked", identification));
    }

    public static UserException userIsDeleted (String identification) {
        return new UserException(String.format("User %s is deleted", identification));
    }
}
