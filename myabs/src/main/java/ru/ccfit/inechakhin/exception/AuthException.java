package ru.ccfit.inechakhin.exception;

public class AuthException extends RuntimeException {
    public AuthException (String message) {
        super(message);
    }

    public static AuthException notCurrentUser () {
        return new AuthException("Is not current user");
    }
}
