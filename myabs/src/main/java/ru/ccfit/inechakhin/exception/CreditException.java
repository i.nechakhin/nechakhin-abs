package ru.ccfit.inechakhin.exception;

public class CreditException extends RuntimeException {
    public CreditException (String message) {
        super(message);
    }

    public static CreditException creditNotMatchRates () {
        return new CreditException("Creating credit does not match existing rates");
    }

    public static CreditException alreadyHaveActiveCredit (String identification) {
        return new CreditException(String.format("Already have an active credit %s", identification));
    }

    public static CreditException noActiveCredit () {
        return new CreditException("No active credit for this account");
    }
}
