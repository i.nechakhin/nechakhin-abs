package ru.ccfit.inechakhin.exception;

public class TransactionException extends RuntimeException {
    public TransactionException (String message) {
        super(message);
    }

    public static TransactionException incorrectVerificationCode () {
        return new TransactionException("Incorrect verification code");
    }

    public static TransactionException typeTransactionNotSupported (String type) {
        return new TransactionException(String.format("This transaction type %s is not supported yet", type));
    }

    public static TransactionException accountNotBelongToCurrentUser () {
        return new TransactionException("Account not belong to current user");
    }

    public static TransactionException incorrectTransactionSource (String source) {
        return new TransactionException(String.format("Incorrect transaction source %s", source));
    }

    public static TransactionException notAvailableTransaction () {
        return new TransactionException("Not enough funds in account");
    }

    public static TransactionException notEnoughMoneyForCredit (String need, String received) {
        return new TransactionException(String.format("Not enough money pay off credit, need: %s, received: %s", need, received));
    }
}
