package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.UserStatus;
import ru.ccfit.inechakhin.database.repository.UserStatusRepository;
import ru.ccfit.inechakhin.service.IUserStatusService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class UserStatusServiceImpl implements IUserStatusService {
    private final UserStatusRepository userStatusRepository;

    public UserStatusServiceImpl (UserStatusRepository userStatusRepository) {
        this.userStatusRepository = userStatusRepository;
    }

    @Override
    @Cacheable("userStatus")
    public UserStatus findByName (String name) {
        return userStatusRepository.findByName(name).orElseThrow(() -> new EntityNotFoundException("User status not found"));
    }

    @Override
    @Cacheable("userStatus")
    public UserStatus findById (Long id) {
        return userStatusRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("User status not found"));
    }
}
