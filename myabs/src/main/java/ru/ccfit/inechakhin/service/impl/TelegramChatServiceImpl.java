package ru.ccfit.inechakhin.service.impl;

import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.TelegramChat;
import ru.ccfit.inechakhin.database.repository.TelegramChatRepository;
import ru.ccfit.inechakhin.service.ITelegramChatService;

import jakarta.persistence.EntityNotFoundException;

import java.util.Optional;

@Service
public class TelegramChatServiceImpl implements ITelegramChatService {
    private final TelegramChatRepository telegramChatRepository;

    public TelegramChatServiceImpl (TelegramChatRepository telegramChatRepository) {
        this.telegramChatRepository = telegramChatRepository;
    }

    @Override
    public TelegramChat save (TelegramChat telegramChatForSave) {
        return telegramChatRepository.save(telegramChatForSave);
    }

    @Override
    public TelegramChat findById (Long id) {
        return findByIdOrThrow(id);
    }

    @Override
    public TelegramChat findByUserId (Long userId) {
        return telegramChatRepository.findByUserId(userId).orElseThrow(EntityNotFoundException::new);
    }

    public void tokenVerification (String token, Long chatId) {
        Optional<TelegramChat> maybeToken = telegramChatRepository.findByToken(token);
        maybeToken.ifPresentOrElse(
                (telegramChat) -> {
                    telegramChat.setChatId(chatId);
                    telegramChatRepository.save(telegramChat);
                    sendVerificationResult(true, chatId);
                },
                () -> sendVerificationResult(false, chatId)
        );
    }

    private void sendVerificationResult (boolean isTokenVerified, Long chatId) {}

    private TelegramChat findByIdOrThrow (Long id) {
        return telegramChatRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Telegram chat not found")
        );
    }
}