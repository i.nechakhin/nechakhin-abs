package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.AccountStatus;
import ru.ccfit.inechakhin.database.repository.AccountStatusRepository;
import ru.ccfit.inechakhin.service.IAccountStatusService;

import jakarta.persistence.EntityNotFoundException;

import java.util.Optional;

@Service
public class AccountStatusServiceImpl implements IAccountStatusService {
    private final AccountStatusRepository accountStatusRepository;

    public AccountStatusServiceImpl (AccountStatusRepository accountStatusRepository) {
        this.accountStatusRepository = accountStatusRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("accountStates")
    public AccountStatus findByName (String name) {
        return Optional.ofNullable(accountStatusRepository.findByName(name)).orElseThrow(() -> new EntityNotFoundException("Account status not found"));
    }

    @Override
    @Transactional
    public AccountStatus createAccountStatus (AccountStatus status) {
        return accountStatusRepository.save(status);
    }
}