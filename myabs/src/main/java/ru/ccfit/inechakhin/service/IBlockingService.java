package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.Blocking;

import java.util.List;

public interface IBlockingService {
    Blocking saveBlocking (Blocking blocking);

    Blocking findBlockingById (Long blockingId);

    List<Blocking> findBlockingByAccountId (Long accountId);
}
