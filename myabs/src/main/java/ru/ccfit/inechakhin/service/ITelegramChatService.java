package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.TelegramChat;

public interface ITelegramChatService {
    TelegramChat save (TelegramChat telegramChatForSave);

    TelegramChat findById (Long id);

    TelegramChat findByUserId (Long userId);

    void tokenVerification (String token, Long chatId);
}
