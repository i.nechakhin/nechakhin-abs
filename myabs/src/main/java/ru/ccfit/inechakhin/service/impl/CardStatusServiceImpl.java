package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.CardStatus;
import ru.ccfit.inechakhin.database.repository.CardStatusRepository;
import ru.ccfit.inechakhin.service.ICardStatusService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class CardStatusServiceImpl implements ICardStatusService {
    private final CardStatusRepository cardStatusRepository;

    public CardStatusServiceImpl (CardStatusRepository cardStatusRepository) {
        this.cardStatusRepository = cardStatusRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("cardStatus")
    public CardStatus findStatusByName (String name) {
        return cardStatusRepository.findByName(name).orElseThrow(() -> new EntityNotFoundException("Card status not found"));
    }

    @Override
    @Transactional
    public CardStatus createCardStatus (CardStatus status) {
        return cardStatusRepository.save(status);
    }
}