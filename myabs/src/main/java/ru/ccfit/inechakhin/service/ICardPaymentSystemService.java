package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.CardPaymentSystem;

public interface ICardPaymentSystemService {
    CardPaymentSystem findCardPaymentSystemByName (String name);
}