package ru.ccfit.inechakhin.service.impl;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.Rates;
import ru.ccfit.inechakhin.database.repository.RatesRepository;
import ru.ccfit.inechakhin.service.IRatesService;

import java.math.BigDecimal;

@Service
public class RatesServiceImpl implements IRatesService {
    private final RatesRepository ratesRepository;

    public RatesServiceImpl (RatesRepository ratesRepository) {
        this.ratesRepository = ratesRepository;
    }

    @Override
    @Transactional
    public Rates saveRates (Rates rates) {
        return ratesRepository.save(rates);
    }

    @Override
    @Transactional
    public void deleteRates (Rates rates) {
        ratesRepository.delete(rates);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("rates")
    public Iterable<Rates> findAllRates (Integer offset, Integer pageSize) {
        return ratesRepository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("rates")
    public Rates findRatesById (Long ratesId) {
        return ratesRepository.findById(ratesId).orElseThrow(() -> new EntityNotFoundException("Rates not found"));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("rates")
    public Rates findRatesByMinMaxAmountAndMinMaxTermAndAnnualRate (BigDecimal minAmount, BigDecimal maxAmount, BigDecimal minTerm, BigDecimal maxTerm, BigDecimal annualRate) {
        return ratesRepository.findByMinAmountAndMaxAmountAndMinTermAndMaxTermAndAnnualRate(minAmount, maxAmount, minTerm, maxTerm, annualRate)
                .orElseThrow(() -> new EntityNotFoundException("Rates not found"));
    }
}
