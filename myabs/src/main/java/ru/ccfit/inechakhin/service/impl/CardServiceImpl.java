package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.Card;
import ru.ccfit.inechakhin.database.repository.CardRepository;
import ru.ccfit.inechakhin.service.ICardService;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;

@Service
public class CardServiceImpl implements ICardService {
    private final CardRepository cardRepository;

    public CardServiceImpl (CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    @Transactional
    public Card saveCard (Card card) {
        return cardRepository.save(card);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("cards")
    public Iterable<Card> findAllCard (Integer offset, Integer pageSize) {
        return cardRepository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("cards")
    public Card findCardById (Long cardId) {
        return cardRepository.findById(cardId).orElseThrow(() -> new EntityNotFoundException("Card not found"));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("cards")
    public Card findCardByNumber (Long number) {
        return cardRepository.findCardByNumber(number);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("cards")
    public List<Card> findCardsByAccountId (Long accountId) {
        return cardRepository.findAllByAccountId(accountId);
    }
}
