package ru.ccfit.inechakhin.service.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.UserCredential;
import ru.ccfit.inechakhin.database.repository.UserCredentialRepository;

@Service
public class UserCredentialServiceImpl implements UserDetailsService {
    private final UserCredentialRepository userCredentialRepository;

    public UserCredentialServiceImpl (UserCredentialRepository userCredentialRepository) {
        this.userCredentialRepository = userCredentialRepository;
    }

    @Override
    public UserDetails loadUserByUsername (String username) throws UsernameNotFoundException {
        return userCredentialRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user"));
    }

    public void setPassword (UserCredential userCredential, String password) {
        userCredential.setPassword(password);
        userCredentialRepository.save(userCredential);
    }
}