package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.Rates;

import java.math.BigDecimal;
import java.util.List;

public interface IRatesService {
    Rates saveRates (Rates rates);

    void deleteRates (Rates rates);

    Iterable<Rates> findAllRates (Integer offset, Integer pageSize);

    Rates findRatesById (Long ratesId);

    Rates findRatesByMinMaxAmountAndMinMaxTermAndAnnualRate (BigDecimal minAmount, BigDecimal maxAmount, BigDecimal minTerm, BigDecimal maxTerm, BigDecimal annualRate);
}
