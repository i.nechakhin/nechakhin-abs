package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.Card;

import java.util.List;

public interface ICardService {
    Card saveCard (Card card);

    Iterable<Card> findAllCard (Integer offset, Integer pageSize);

    Card findCardById (Long cardId);

    Card findCardByNumber (Long number);

    List<Card> findCardsByAccountId (Long accountId);
}