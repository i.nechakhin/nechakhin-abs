package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.Transaction;
import ru.ccfit.inechakhin.database.repository.TransactionRepository;
import ru.ccfit.inechakhin.service.ITransactionService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class TransactionServiceImpl implements ITransactionService {
    private final TransactionRepository transactionRepository;

    public TransactionServiceImpl (TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    @Cacheable("transaction")
    public Iterable<Transaction> findAllTransactionByAccountId (Long accountId, Integer offset, Integer pageSize) {
        return transactionRepository.findAllByAccountFrom_Id(accountId, PageRequest.of(offset, pageSize));
    }

    @Override
    @Cacheable("transaction")
    public Iterable<Transaction> findAllTransactionByAccountIdAndStatusId (Long accountId, Long statusId, Integer offset, Integer pageSize) {
        return transactionRepository.findAllByAccountFrom_IdAndStatus_Id(accountId, statusId, PageRequest.of(offset, pageSize));
    }

    @Override
    @Cacheable("transaction")
    public Iterable<Transaction> findAllTransactionByAccountIdAndTypeId (Long accountId, Long typeId, Integer offset, Integer pageSize) {
        return transactionRepository.findAllByAccountFrom_IdAndType_Id(accountId, typeId, PageRequest.of(offset, pageSize));
    }

    @Override
    @Cacheable("transaction")
    public Iterable<Transaction> findAllTransactionByAccountIdAndSourceFromId (Long accountId, Long sourceFromId, Integer offset, Integer pageSize) {
        return transactionRepository.findAllByAccountFrom_IdAndSourceFrom_Id(accountId, sourceFromId, PageRequest.of(offset, pageSize));
    }

    @Override
    @Cacheable("transaction")
    public Iterable<Transaction> findAllTransactionByAccountIdAndSourceToId (Long accountId, Long sourceToId, Integer offset, Integer pageSize) {
        return transactionRepository.findAllByAccountFrom_IdAndSourceTo_Id(accountId, sourceToId, PageRequest.of(offset, pageSize));
    }

    @Override
    @Cacheable("transaction")
    public Transaction findTransactionById (Long id) {
        return transactionRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Transaction not found"));
    }

    @Override
    public Transaction save (Transaction transactionForCreate) {
        return transactionRepository.save(transactionForCreate);
    }
}
