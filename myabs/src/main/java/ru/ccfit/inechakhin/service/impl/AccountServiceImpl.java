package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.Account;
import ru.ccfit.inechakhin.database.repository.AccountRepository;
import ru.ccfit.inechakhin.service.IAccountService;

import jakarta.persistence.EntityNotFoundException;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountServiceImpl implements IAccountService {
    private final AccountRepository accountRepository;

    public AccountServiceImpl (AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("accounts")
    public Iterable<Account> findAllAccounts (Integer offset, Integer pageSize) {
        return accountRepository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("accounts")
    public Account findAccountById (Long id) {
        return accountRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Account not found"));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("accounts")
    public List<Account> findAccountsByUserId (Long userId) {
        return accountRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("accounts")
    public List<Account> findAccountsByUserIdAndStatusId (Long userId, Long statusId) {
        return accountRepository.findAccountsByUserIdAndStatusId(userId, statusId);
    }

    @Override
    @Transactional
    public Account saveAccount (Account account) {
        return accountRepository.save(account);
    }

    @Override
    @Transactional
    public boolean transferIsNotAvailable (Account account, BigDecimal amountToTransfer) {
        return amountToTransfer.compareTo(account.getAmount()) > 0;
    }

    @Override
    @Transactional
    public void subtractMoney (Account account, BigDecimal amount) {
        account.subtractMoney(amount);
        accountRepository.save(account);
    }

    @Override
    @Transactional
    public void addMoney (Account account, BigDecimal amount) {
        account.addMoney(amount);
        accountRepository.save(account);
    }

    @Override
    @Transactional
    public void transferMoney (Account accountFrom, Account accountTo, BigDecimal amountForTransfer) {
        accountFrom.subtractMoney(amountForTransfer);
        accountTo.addMoney(amountForTransfer);
        accountRepository.save(accountFrom);
        accountRepository.save(accountTo);
    }
}
