package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.Account;

import java.math.BigDecimal;
import java.util.List;

public interface IAccountService {
    Iterable<Account> findAllAccounts (Integer offset, Integer pageSize);

    Account findAccountById (Long id);

    List<Account> findAccountsByUserId (Long userId);
    
    List<Account> findAccountsByUserIdAndStatusId (Long userId, Long statusId);

    Account saveAccount (Account account);

    boolean transferIsNotAvailable (Account account, BigDecimal amountToTransfer);

    void subtractMoney (Account account, BigDecimal amount);

    void addMoney (Account account, BigDecimal amount);

    void transferMoney (Account accountFrom, Account accountTo, BigDecimal amountForTransfer);
}
