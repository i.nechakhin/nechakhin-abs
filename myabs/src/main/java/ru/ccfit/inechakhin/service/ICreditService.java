package ru.ccfit.inechakhin.service;

import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.Credit;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface ICreditService {
    Credit saveCredit (Credit credit);

    Iterable<Credit> findAllCredits (Integer offset, Integer pageSize);

    Credit findCreditById (Long creditId);

    List<Credit> findCreditByAccountId (Long accountId);

    List<Credit> findCreditByAccountIdAndStatusId (Long accountId, Long statusId);

    void subtractRemainingAmount (Credit credit, BigDecimal amount);

    @Transactional
    void subtractRemainingTerm (Credit credit, BigDecimal term);

    LocalDateTime getActualPaymentDate (Credit actualCredit);

    BigDecimal getMonthlyAnnualRate (Credit actualCredit);

    BigDecimal getMonthlyPayment (Credit actualCredit);

    BigDecimal getMonthlyOverpayment (Credit actualCredit);

    BigDecimal getPaymentForRepaymentCredit (Credit actualCredit);
}
