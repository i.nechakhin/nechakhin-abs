package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.UserStatus;

public interface IUserStatusService {
    UserStatus findByName (String name);

    UserStatus findById (Long id);
}