package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.User;
import ru.ccfit.inechakhin.database.repository.UserRepository;
import ru.ccfit.inechakhin.service.IUserService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class UserServiceImpl implements IUserService {
    private final UserRepository userRepository;

    public UserServiceImpl (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("users")
    public Iterable<User> findAllUsers (Integer offset, Integer pageSize) {
        return userRepository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("users")
    public User findUserById (Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("User not found"));
    }

    @Override
    @Transactional
    public User saveUser (User userForSave) {
        return userRepository.save(userForSave);
    }
}
