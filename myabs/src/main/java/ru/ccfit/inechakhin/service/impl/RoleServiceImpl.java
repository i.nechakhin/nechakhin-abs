package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.Role;
import ru.ccfit.inechakhin.database.repository.RoleRepository;
import ru.ccfit.inechakhin.service.IRoleService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class RoleServiceImpl implements IRoleService {
    private final RoleRepository roleRepository;

    public RoleServiceImpl (RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    @Cacheable("roles")
    public Role findRoleByName (String name) {
        return roleRepository.findByName(name).orElseThrow(EntityNotFoundException::new);
    }
}