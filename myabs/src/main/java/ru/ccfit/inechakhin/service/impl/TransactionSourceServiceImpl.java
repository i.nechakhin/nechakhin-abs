package ru.ccfit.inechakhin.service.impl;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.TransactionSource;
import ru.ccfit.inechakhin.database.repository.TransactionSourceRepository;
import ru.ccfit.inechakhin.service.ITransactionSourceService;

import java.util.List;

@Service
public class TransactionSourceServiceImpl implements ITransactionSourceService {
    private final TransactionSourceRepository transactionSourceRepository;

    public TransactionSourceServiceImpl (TransactionSourceRepository transactionSourceRepository) {
        this.transactionSourceRepository = transactionSourceRepository;
    }

    @Override
    @Cacheable("transactionSource")
    public List<TransactionSource> findAll () {
        return transactionSourceRepository.findAll();
    }

    @Override
    @Cacheable("transactionSource")
    public TransactionSource findById (Long id) {
        return transactionSourceRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Transaction source not found"));
    }

    @Override
    @Cacheable("transactionSource")
    public TransactionSource findByName (String name) {
        return transactionSourceRepository.findByName(name).orElseThrow(() -> new EntityNotFoundException("Transaction source not found"));
    }
}
