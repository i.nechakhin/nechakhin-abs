package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.TransactionStatus;

public interface ITransactionStatusService {
    TransactionStatus findById (Long id);

    TransactionStatus findByName (String name);
}