package ru.ccfit.inechakhin.service;

public interface ICurrentUserService {
    Long getCurrentUserId ();
}
