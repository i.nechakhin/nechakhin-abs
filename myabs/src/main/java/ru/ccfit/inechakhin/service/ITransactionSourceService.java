package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.TransactionSource;

import java.util.List;

public interface ITransactionSourceService {
    List<TransactionSource> findAll ();

    TransactionSource findById (Long id);

    TransactionSource findByName (String name);
}
