package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.User;

public interface IUserService {
    Iterable<User> findAllUsers (Integer offset, Integer pageSize);

    User findUserById (Long userId);

    User saveUser (User useForCreate);
}