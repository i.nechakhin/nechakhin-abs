package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.AccountStatus;

public interface IAccountStatusService {
    AccountStatus findByName (String name);

    AccountStatus createAccountStatus (AccountStatus status);
}