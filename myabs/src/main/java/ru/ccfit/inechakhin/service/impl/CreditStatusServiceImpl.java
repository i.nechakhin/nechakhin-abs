package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.CreditStatus;
import ru.ccfit.inechakhin.database.repository.CreditStatusRepository;
import ru.ccfit.inechakhin.service.ICreditStatusService;

import jakarta.persistence.EntityNotFoundException;

import java.util.Optional;

@Service
public class CreditStatusServiceImpl implements ICreditStatusService {
    private final CreditStatusRepository creditStatusRepository;

    public CreditStatusServiceImpl (CreditStatusRepository creditStatusRepository) {
        this.creditStatusRepository = creditStatusRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("creditStatus")
    public CreditStatus findStatusById (Long id) {
        return creditStatusRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Credit status not found"));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("creditStatus")
    public CreditStatus findStatusByName (String name) {
        return Optional.ofNullable(creditStatusRepository.findByName(name)).orElseThrow(() -> new EntityNotFoundException("Credit status not found"));
    }

    @Override
    @Transactional
    public CreditStatus createCreditStatus (CreditStatus status) {
        return creditStatusRepository.save(status);
    }
}
