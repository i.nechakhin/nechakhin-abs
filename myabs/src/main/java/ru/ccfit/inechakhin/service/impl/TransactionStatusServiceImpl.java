package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.TransactionStatus;
import ru.ccfit.inechakhin.database.repository.TransactionStatusRepository;
import ru.ccfit.inechakhin.service.ITransactionStatusService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class TransactionStatusServiceImpl implements ITransactionStatusService {
    private final TransactionStatusRepository transactionStatusRepository;

    public TransactionStatusServiceImpl (TransactionStatusRepository transactionStatusRepository) {
        this.transactionStatusRepository = transactionStatusRepository;
    }

    @Override
    @Cacheable("transactionStatus")
    public TransactionStatus findById (Long id) {
        return transactionStatusRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Transaction status not found"));
    }

    @Override
    @Cacheable("transactionStatus")
    public TransactionStatus findByName (String name) {
        return transactionStatusRepository.findByName(name).orElseThrow(() -> new EntityNotFoundException("Transaction status not found"));
    }
}