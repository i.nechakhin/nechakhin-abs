package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.Blocking;
import ru.ccfit.inechakhin.database.repository.BlockingRepository;
import ru.ccfit.inechakhin.service.IBlockingService;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;

@Service
public class BlockingServiceImpl implements IBlockingService {
    private final BlockingRepository blockingRepository;

    public BlockingServiceImpl (BlockingRepository blockingRepository) {
        this.blockingRepository = blockingRepository;
    }

    @Override
    @Transactional
    public Blocking saveBlocking (Blocking blocking) {
        return blockingRepository.save(blocking);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("blocking")
    public Blocking findBlockingById (Long blockingId) {
        return blockingRepository.findById(blockingId).orElseThrow(() -> new EntityNotFoundException("Blocking not found"));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("blocking")
    public List<Blocking> findBlockingByAccountId (Long accountId) {
        return blockingRepository.findAllByAccountId(accountId);
    }
}
