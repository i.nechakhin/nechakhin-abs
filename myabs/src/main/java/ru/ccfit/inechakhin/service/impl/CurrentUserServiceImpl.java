package ru.ccfit.inechakhin.service.impl;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.UserCredential;
import ru.ccfit.inechakhin.service.ICurrentUserService;

@Service
public class CurrentUserServiceImpl implements ICurrentUserService {
    @Override
    public Long getCurrentUserId () {
        UserCredential userCredential = (UserCredential) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userCredential.getUser().getId();
    }
}