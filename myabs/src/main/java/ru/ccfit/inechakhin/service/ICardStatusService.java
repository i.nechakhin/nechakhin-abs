package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.CardStatus;

public interface ICardStatusService {
    CardStatus findStatusByName (String name);

    CardStatus createCardStatus (CardStatus status);
}