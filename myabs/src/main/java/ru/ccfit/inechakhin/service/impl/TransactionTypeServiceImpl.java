package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.TransactionType;
import ru.ccfit.inechakhin.database.repository.TransactionTypeRepository;
import ru.ccfit.inechakhin.service.ITransactionTypeService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class TransactionTypeServiceImpl implements ITransactionTypeService {
    private final TransactionTypeRepository transactionTypeRepository;

    public TransactionTypeServiceImpl (TransactionTypeRepository transactionTypeRepository) {
        this.transactionTypeRepository = transactionTypeRepository;
    }

    @Override
    @Cacheable("transactionType")
    public TransactionType findById (Long id) {
        return transactionTypeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Transaction type not found"));
    }

    @Override
    @Cacheable("transactionType")
    public TransactionType findByName (String name) {
        return transactionTypeRepository.findByName(name).orElseThrow(() -> new EntityNotFoundException("Transaction type not found"));
    }
}
