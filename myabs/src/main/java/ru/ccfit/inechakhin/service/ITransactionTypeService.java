package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.TransactionType;

public interface ITransactionTypeService {
    TransactionType findById (Long id);

    TransactionType findByName (String name);
}