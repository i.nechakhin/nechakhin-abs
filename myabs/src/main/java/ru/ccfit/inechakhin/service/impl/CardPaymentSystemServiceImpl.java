package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.CardPaymentSystem;
import ru.ccfit.inechakhin.database.repository.CardPaymentSystemRepository;
import ru.ccfit.inechakhin.service.ICardPaymentSystemService;

import jakarta.persistence.EntityNotFoundException;

import java.util.Optional;

@Service
public class CardPaymentSystemServiceImpl implements ICardPaymentSystemService {
    private final CardPaymentSystemRepository cardPaymentSystemRepository;

    public CardPaymentSystemServiceImpl (CardPaymentSystemRepository cardPaymentSystemRepository) {
        this.cardPaymentSystemRepository = cardPaymentSystemRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("cardPaymentSystem")
    public CardPaymentSystem findCardPaymentSystemByName (String name) {
        return Optional.ofNullable(cardPaymentSystemRepository.findByName(name)).orElseThrow(() -> new EntityNotFoundException("Card payment system not found"));
    }
}
