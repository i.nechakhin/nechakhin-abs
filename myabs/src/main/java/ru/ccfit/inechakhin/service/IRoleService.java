package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.Role;

public interface IRoleService {
    Role findRoleByName (String name);
}