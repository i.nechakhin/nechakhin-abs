package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.Transaction;

import java.util.List;

public interface ITransactionService {
    Iterable<Transaction> findAllTransactionByAccountId (Long accountId, Integer offset, Integer pageSize);

    Iterable<Transaction> findAllTransactionByAccountIdAndStatusId (Long accountId, Long statusId, Integer offset, Integer pageSize);

    Iterable<Transaction> findAllTransactionByAccountIdAndTypeId (Long accountId, Long typeId, Integer offset, Integer pageSize);

    Iterable<Transaction> findAllTransactionByAccountIdAndSourceFromId (Long accountId, Long sourceFromId, Integer offset, Integer pageSize);

    Iterable<Transaction> findAllTransactionByAccountIdAndSourceToId (Long accountId, Long sourceToId, Integer offset, Integer pageSize);

    Transaction findTransactionById (Long id);

    Transaction save (Transaction transactionForSave);
}
