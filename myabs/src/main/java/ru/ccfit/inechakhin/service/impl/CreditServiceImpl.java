package ru.ccfit.inechakhin.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.Credit;
import ru.ccfit.inechakhin.database.repository.CreditRepository;
import ru.ccfit.inechakhin.service.ICreditService;

import jakarta.persistence.EntityNotFoundException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class CreditServiceImpl implements ICreditService {
    private final CreditRepository creditRepository;

    public CreditServiceImpl (CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    @Override
    @Transactional
    public Credit saveCredit (Credit credit) {
        return creditRepository.save(credit);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("credits")
    public Iterable<Credit> findAllCredits (Integer offset, Integer pageSize) {
        return creditRepository.findAll(PageRequest.of(offset, pageSize));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("credits")
    public Credit findCreditById (Long creditId) {
        return creditRepository.findById(creditId).orElseThrow(() -> new EntityNotFoundException("Credit not found"));
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("credits")
    public List<Credit> findCreditByAccountId (Long accountId) {
        return creditRepository.findAllByAccountId(accountId);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("credits")
    public List<Credit> findCreditByAccountIdAndStatusId (Long accountId, Long statusId) {
        return creditRepository.findCreditsByAccountIdAndStatusId(accountId, statusId);
    }

    @Override
    @Transactional
    public void subtractRemainingAmount (Credit credit, BigDecimal amount) {
        credit.subtractRemainingAmount(amount);
        creditRepository.save(credit);
    }

    @Override
    @Transactional
    public void subtractRemainingTerm (Credit credit, BigDecimal term) {
        credit.subtractRemainingTerm(term);
        creditRepository.save(credit);
    }

    @Override
    public LocalDateTime getActualPaymentDate (Credit actualCredit) {
        LocalDateTime dateStartCredit = actualCredit.getCreatedAt();
        LocalDateTime actualDate = LocalDateTime.now();
        if (actualDate.getDayOfMonth() > dateStartCredit.getDayOfMonth()) {
            if (actualDate.getMonthValue() == 12) {
                return LocalDateTime.of(actualDate.getYear() + 1, 1,
                        dateStartCredit.getDayOfMonth(), 0, 0);
            } else {
                return LocalDateTime.of(actualDate.getYear(), actualDate.getMonthValue() + 1,
                        dateStartCredit.getDayOfMonth(), 0, 0);
            }
        } else {
            return LocalDateTime.of(actualDate.getYear(), actualDate.getMonthValue(),
                    dateStartCredit.getDayOfMonth(), 0, 0);
        }
    }

    @Override
    public BigDecimal getMonthlyAnnualRate (Credit actualCredit) {
        return actualCredit.getAnnualRate().divide(BigDecimal.valueOf(100.0 * 12.0), 2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getMonthlyPayment (Credit actualCredit) {
        BigDecimal monthlyAnnualRate = getMonthlyAnnualRate(actualCredit);
        BigDecimal dividend = actualCredit.getRemainingAmount().multiply(monthlyAnnualRate);
        BigDecimal one = BigDecimal.ONE;
        BigDecimal temp = one.add(monthlyAnnualRate).pow(actualCredit.getRemainingTerm().intValue());
        BigDecimal divisor = one.subtract(one.divide(temp, 2, RoundingMode.HALF_UP));
        return dividend.divide(divisor, 2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getMonthlyOverpayment (Credit actualCredit) {
        BigDecimal monthlyAnnualRate = getMonthlyAnnualRate(actualCredit);
        return actualCredit.getRemainingAmount().multiply(monthlyAnnualRate);
    }

    @Override
    public BigDecimal getPaymentForRepaymentCredit (Credit actualCredit) {
        LocalDateTime actualPaymentDate = getActualPaymentDate(actualCredit);
        LocalDateTime nowDate = LocalDateTime.now();
        long days = ChronoUnit.DAYS.between(actualPaymentDate, nowDate);
        BigDecimal overpayment = BigDecimal.valueOf((31.0 - days) / 31.0);
        BigDecimal interestPayment = getMonthlyPayment(actualCredit).multiply(overpayment);
        return actualCredit.getRemainingAmount().add(interestPayment);
    }
}
