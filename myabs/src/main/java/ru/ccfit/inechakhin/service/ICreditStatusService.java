package ru.ccfit.inechakhin.service;

import ru.ccfit.inechakhin.database.entity.CreditStatus;

public interface ICreditStatusService {
    CreditStatus findStatusById (Long id);

    CreditStatus findStatusByName (String name);

    CreditStatus createCreditStatus (CreditStatus status);
}
