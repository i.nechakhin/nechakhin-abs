package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.Card;
import ru.ccfit.inechakhin.web.dto.card.CardInfoDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CardMapper {
    @Mapping(source = "account.id", target = "accountId")
    @Mapping(source = "number", target = "number")
    @Mapping(source = "expDate", target = "expDate")
    @Mapping(source = "cvv", target = "cvv")
    @Mapping(source = "paymentSystem.name", target = "paymentSystem")
    @Mapping(source = "status.name", target = "status")
    CardInfoDto cardToCardInfoDto (Card card);

    List<CardInfoDto> mapListCardToListCardInfoDto (List<Card> entity);

    @Named("maskedNumber")
    default String numberToMaskedNumber (Long number) {
        return number.toString().replaceAll(".(?=.{4})", "*");
    }
}