package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.Blocking;
import ru.ccfit.inechakhin.web.dto.blocking.BlockingCreateDto;
import ru.ccfit.inechakhin.web.dto.blocking.BlockingInfoDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BlockingMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "account", ignore = true)
    @Mapping(source = "startBlock", target = "startBlock")
    @Mapping(source = "endBlock", target = "endBlock")
    Blocking blockingCreateDtoToBlocking (BlockingCreateDto blockingCreateDto);

    @Mapping(source = "account.id", target = "accountId")
    @Mapping(source = "startBlock", target = "startBlock")
    @Mapping(source = "endBlock", target = "endBlock")
    BlockingInfoDto blockingToBlockingInfoDto (Blocking blocking);

    List<BlockingInfoDto> listBlockingToListBlockingInfoDto (List<Blocking> entity);
}
