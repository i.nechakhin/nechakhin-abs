package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.Account;
import ru.ccfit.inechakhin.web.dto.account.AccountInfoDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "amount", target = "amount")
    @Mapping(source = "updatedAt", target = "updatedAt")
    @Mapping(source = "createdAt", target = "createdAt")
    @Mapping(source = "rates.id", target = "ratesId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "status.name", target = "statusName")
    AccountInfoDto accountToAccountInfoDto (Account entity);

    List<AccountInfoDto> mapListAccountToListAccountInfoDto (List<Account> entity);
}