package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.TransactionSource;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionSourceDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionSourceMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "transactionsFrom", ignore = true)
    @Mapping(target = "transactionsTo", ignore = true)
    @Mapping(source = "name", target = "name")
    @Mapping(source = "commission", target = "commission")
    TransactionSource transactionSourceDtoToTransactionSource (TransactionSourceDto dto);

    @Mapping(source = "name", target = "name")
    @Mapping(source = "commission", target = "commission")
    TransactionSourceDto TransactionSourceToTransactionSourceDto (TransactionSource entity);

    List<TransactionSourceDto> mapListTransactionSourceToListTransactionSourceDto (List<TransactionSource> entity);
}
