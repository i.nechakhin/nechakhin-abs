package ru.ccfit.inechakhin.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ccfit.inechakhin.database.entity.TelegramChat;
import ru.ccfit.inechakhin.web.dto.telegramchat.TelegramChatTokenDto;

@Mapper(componentModel = "spring")
public interface TelegramChatMapper {
    @Mapping(source = "token", target = "token")
    TelegramChatTokenDto telegramChatToTelegramTokenDto (TelegramChat telegramChat);
}
