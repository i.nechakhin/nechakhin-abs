package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.Rates;
import ru.ccfit.inechakhin.web.dto.account.AccountRatesDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RatesMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "transactionsTo", ignore = true)
    @Mapping(source = "minAmount", target = "minAmount")
    @Mapping(source = "maxAmount", target = "maxAmount")
    @Mapping(source = "minTerm", target = "minTerm")
    @Mapping(source = "maxTerm", target = "maxTerm")
    @Mapping(source = "annualRate", target = "annualRate")
    Rates accountRatesDtoToRates (AccountRatesDto accountRatesDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "transactionsTo", ignore = true)
    @Mapping(source = "minAmount", target = "minAmount")
    @Mapping(source = "maxAmount", target = "maxAmount")
    @Mapping(source = "minTerm", target = "minTerm")
    @Mapping(source = "maxTerm", target = "maxTerm")
    @Mapping(source = "annualRate", target = "annualRate")
    Rates update (AccountRatesDto accountRatesDto, @MappingTarget Rates rates);

    @Mapping(source = "minAmount", target = "minAmount")
    @Mapping(source = "maxAmount", target = "maxAmount")
    @Mapping(source = "minTerm", target = "minTerm")
    @Mapping(source = "maxTerm", target = "maxTerm")
    @Mapping(source = "annualRate", target = "annualRate")
    AccountRatesDto ratesToAccountRatesDto (Rates rates);

    List<AccountRatesDto> mapListRatesToListAccountRatesDto (List<Rates> entity);
}
