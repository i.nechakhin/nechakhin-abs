package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.Credit;
import ru.ccfit.inechakhin.web.dto.credit.CreditCreateUpdateDto;
import ru.ccfit.inechakhin.web.dto.credit.CreditInfoDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CreditMapper {
    @Mapping(source = "account.id", target = "accountId")
    @Mapping(source = "remainingAmount", target = "remainingAmount")
    @Mapping(source = "remainingTerm", target = "remainingTerm")
    @Mapping(source = "annualRate", target = "annualRate")
    @Mapping(source = "status.name", target = "status")
    CreditInfoDto creditToCreditInfoDto (Credit credit);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "account", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(source = "remainingAmount", target = "remainingAmount")
    @Mapping(source = "remainingTerm", target = "remainingTerm")
    @Mapping(source = "annualRate", target = "annualRate")
    Credit creditCreateUpdateDtoToCredit (CreditCreateUpdateDto creditCreateUpdateDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "account", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(source = "remainingAmount", target = "remainingAmount")
    @Mapping(source = "remainingTerm", target = "remainingTerm")
    @Mapping(source = "annualRate", target = "annualRate")
    Credit update (CreditCreateUpdateDto creditCreateUpdateDto, @MappingTarget Credit credit);

    List<CreditInfoDto> mapListCreditToListCreditInfoDto (List<Credit> entity);
}
