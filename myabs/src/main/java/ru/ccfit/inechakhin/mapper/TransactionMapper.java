package ru.ccfit.inechakhin.mapper;

import org.mapstruct.*;
import ru.ccfit.inechakhin.database.entity.Transaction;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionCreateDto;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionInfoDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "accountTo", ignore = true)
    @Mapping(target = "accountFrom", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "verificationCode", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "amount", source = "amount")
    @Mapping(target = "accountFrom.id", source = "accountFromId")
    @Mapping(target = "accountTo.id", source = "accountToId")
    @Mapping(target = "sourceFrom.id", source = "sourceFromId")
    @Mapping(target = "sourceTo.id", source = "sourceToId")
    @Mapping(target = "type.id", source = "typeId")
    Transaction transactionCreateDtoToTransaction(TransactionCreateDto transactionCreateDto);

    @Mapping(target = "amount", source = "amount")
    @Mapping(target = "accountFromId", source = "accountFrom.id")
    @Mapping(target = "accountToId", source = "accountTo.id")
    @Mapping(target = "sourceFromId", source = "sourceFrom.id")
    @Mapping(target = "sourceToId", source = "sourceTo.id")
    @Mapping(target = "status", source = "status.name")
    @Mapping(target = "type", source = "type.name")
    TransactionInfoDto transactionToTransactionInfoDto(Transaction transaction);

    List<TransactionInfoDto> mapListTransactionToListTransactionInfoDto (List<Transaction> transactionList);
}
