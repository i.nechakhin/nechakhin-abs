package ru.ccfit.inechakhin.facade;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.exception.CreditException;
import ru.ccfit.inechakhin.exception.TransactionException;
import ru.ccfit.inechakhin.exception.UserException;
import ru.ccfit.inechakhin.mapper.TransactionMapper;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionCreateDto;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionInfoDto;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionVerificationCodeDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;

@Service
public class TransactionFacade {
    private final IAccountService accountService;
    private final ITransactionService transactionService;
    private final ITransactionStatusService transactionStatusService;
    private final ITransactionTypeService transactionTypeService;
    private final ITransactionSourceService transactionSourceService;
    private final IUserStatusService userStatusService;
    private final ICurrentUserService currentUserService;
    private final IUserService userService;
    private final ICreditService creditService;
    private final ICreditStatusService creditStatusService;
    private final TransactionMapper transactionMapper;

    public TransactionFacade (IAccountService accountService, ITransactionService transactionService,
                              ITransactionStatusService transactionStatusService, ITransactionTypeService transactionTypeService,
                              ITransactionSourceService transactionSourceService, IUserStatusService userStatusService,
                              ICurrentUserService currentUserService, IUserService userService, ICreditService creditService,
                              ICreditStatusService creditStatusService, TransactionMapper transactionMapper) {
        this.accountService = accountService;
        this.transactionService = transactionService;
        this.transactionStatusService = transactionStatusService;
        this.transactionTypeService = transactionTypeService;
        this.transactionSourceService = transactionSourceService;
        this.userStatusService = userStatusService;
        this.currentUserService = currentUserService;
        this.userService = userService;
        this.creditService = creditService;
        this.creditStatusService = creditStatusService;
        this.transactionMapper = transactionMapper;
    }

    @Transactional
    public TransactionInfoDto createTransaction (TransactionCreateDto transactionCreateRequest) {
        Account accountFrom = accountService.findAccountById(transactionCreateRequest.getAccountFromId());
        Account accountTo = accountService.findAccountById(transactionCreateRequest.getAccountToId());
        User currentUser = userService.findUserById(currentUserService.getCurrentUserId());
        User userTo = userService.findUserById(accountTo.getUser().getId());

        isUserBlockedOrDeleted(currentUser);
        isUserBlockedOrDeleted(userTo);
        accountFromBelongToCurrentUser(accountFrom, currentUser);

        TransactionStatus createStatus = transactionStatusService.findByName(TransactionStatus.Name.IN_PROCESS.name());
        Transaction transactionForCreate = transactionMapper.transactionCreateDtoToTransaction(transactionCreateRequest);
        transactionForCreate.setStatus(createStatus);
        transactionForCreate.setVerificationCode(generateVerificationCode());
        Transaction createdTransaction = transactionService.save(transactionForCreate);
        return transactionMapper.transactionToTransactionInfoDto(createdTransaction);
    }

    @Transactional
    public TransactionInfoDto checkVerificationCode (Long id, TransactionVerificationCodeDto transactionVerificationCodeRequest) {
        Transaction transactionForVerification = transactionService.findTransactionById(id);

        Integer correctVerificationCode = transactionForVerification.getVerificationCode();
        Integer inputVerificationCode = transactionVerificationCodeRequest.getVerificationCode();
        if (Objects.equals(correctVerificationCode, inputVerificationCode)) {
            return makeTransaction(id);
        } else {
            throw TransactionException.incorrectVerificationCode();
        }
    }

    @Transactional
    public TransactionInfoDto makeTransaction (Long id) {
        Transaction transactionToDo = transactionService.findTransactionById(id);

        TransactionStatus declineStatus = transactionStatusService.findByName(TransactionStatus.Name.DECLINE.name());

        TransactionSource currentSourceFrom = transactionSourceService.findById(transactionToDo.getSourceFrom().getId());
        TransactionSource currentSourceTo = transactionSourceService.findById(transactionToDo.getSourceTo().getId());
        TransactionSource bankAccountSource = transactionSourceService.findByName(TransactionSource.Name.BANK_ACCOUNT.name());

        TransactionType currentType = transactionTypeService.findById(transactionToDo.getType().getId());
        TransactionType creditType = transactionTypeService.findByName(TransactionType.Name.CREDIT.name());
        TransactionType transferType = transactionTypeService.findByName(TransactionType.Name.TRANSFER.name());

        BigDecimal amountForTransfer = transactionToDo.getAmount();
        Account accountFrom = accountService.findAccountById(transactionToDo.getAccountFrom().getId());
        Account accountTo = accountService.findAccountById(transactionToDo.getAccountTo().getId());
        if (Objects.equals(currentType, creditType)) {
            Credit activeCredit = foundActiveCredit(accountTo.getId());
            if (!Objects.equals(currentSourceTo, bankAccountSource)) {
                transactionToDo.setStatus(declineStatus);
                transactionService.save(transactionToDo);
                throw TransactionException.incorrectTransactionSource(currentSourceTo.getName());
            }
            if (Objects.equals(currentSourceFrom, bankAccountSource)) {
                if (accountService.transferIsNotAvailable(accountFrom, amountForTransfer)) {
                    transactionToDo.setStatus(declineStatus);
                    transactionService.save(transactionToDo);
                    throw TransactionException.notAvailableTransaction();
                }
            } // else source transaction always available

            if (equalsDate(transactionToDo.getCreatedAt(), creditService.getActualPaymentDate(activeCredit))) {
                BigDecimal monthlyPayment = creditService.getMonthlyPayment(activeCredit);
                BigDecimal monthlyOverpayment = creditService.getMonthlyOverpayment(activeCredit);
                if (amountForTransfer.compareTo(monthlyPayment) < 0) {
                    transactionToDo.setStatus(declineStatus);
                    transactionService.save(transactionToDo);
                    throw TransactionException.notEnoughMoneyForCredit(monthlyPayment.toString(), amountForTransfer.toString());
                } else {
                    subtractMoneyFromSource(accountFrom, amountForTransfer, currentSourceFrom);
                    creditService.subtractRemainingAmount(activeCredit, amountForTransfer.subtract(monthlyOverpayment));
                    creditService.subtractRemainingTerm(activeCredit, BigDecimal.valueOf(1.0));
                }
            } else {
                subtractMoneyFromSource(accountFrom, amountForTransfer, currentSourceFrom);
                creditService.subtractRemainingAmount(activeCredit, amountForTransfer);
            }
            if (activeCredit.getRemainingAmount().compareTo(BigDecimal.valueOf(1.0)) <= 0) {
                activeCredit.setStatus(creditStatusService.findStatusByName(CreditStatus.Name.CLOSED.name()));
                creditService.saveCredit(activeCredit);
            }
        } else if (Objects.equals(currentType, transferType)) {
            if (Objects.equals(currentSourceFrom, bankAccountSource)) {
                if (accountService.transferIsNotAvailable(accountFrom, amountForTransfer)) {
                    transactionToDo.setStatus(declineStatus);
                    transactionService.save(transactionToDo);
                    throw TransactionException.notAvailableTransaction();
                }
            } // else source transaction always available

            if (Objects.equals(currentSourceFrom, bankAccountSource) && Objects.equals(currentSourceTo, bankAccountSource)) {
                accountService.transferMoney(accountFrom, accountTo, amountForTransfer);
            } else if (Objects.equals(currentSourceFrom, bankAccountSource)) {
                subtractMoneyFromSource(accountFrom, amountForTransfer, currentSourceFrom);
                // add money to another source
            } else if (Objects.equals(currentSourceTo, bankAccountSource)) {
                // subtract money from another source
                addMoneyToSource(accountTo, amountForTransfer, currentSourceTo);
            } else {
                transactionToDo.setStatus(declineStatus);
                transactionService.save(transactionToDo);
                throw TransactionException.incorrectTransactionSource("");
            }
        } else {
            transactionToDo.setStatus(declineStatus);
            transactionService.save(transactionToDo);
            throw TransactionException.typeTransactionNotSupported(currentType.getName());
        }

        transactionToDo.setStatus(transactionStatusService.findByName(TransactionStatus.Name.SUCCESS.name()));
        Transaction resultTransaction = transactionService.save(transactionToDo);
        return transactionMapper.transactionToTransactionInfoDto(resultTransaction);
    }

    private BigDecimal getSumAfterCommission (BigDecimal amount, TransactionSource source) {
        BigDecimal commission = source.getCommission().divide(BigDecimal.valueOf(100.0), 2, RoundingMode.HALF_UP);
        BigDecimal sumOfCommission = amount.multiply(commission);
        return amount.subtract(sumOfCommission);
    }

    private void addMoneyToSource (Account account, BigDecimal amount, TransactionSource source) {
        BigDecimal finalAmount = getSumAfterCommission(amount, source);
        TransactionSource currentSource = transactionSourceService.findById(source.getId());
        TransactionSource bankAccountSource = transactionSourceService.findByName(TransactionSource.Name.BANK_ACCOUNT.name());
        if (Objects.equals(currentSource, bankAccountSource)) {
            accountService.addMoney(account, finalAmount);
        } // else another source
    }

    private void subtractMoneyFromSource (Account account, BigDecimal amount, TransactionSource source) {
        BigDecimal finalAmount = getSumAfterCommission(amount, source);
        TransactionSource currentSource = transactionSourceService.findById(source.getId());
        TransactionSource bankAccountSource = transactionSourceService.findByName(TransactionSource.Name.BANK_ACCOUNT.name());
        if (Objects.equals(currentSource, bankAccountSource)) {
            accountService.subtractMoney(account, finalAmount);
        } // else another source
    }

    private void isUserBlockedOrDeleted (User user) {
        if (Objects.equals(user.getStatus(), userStatusService.findByName(UserStatus.Name.BLOCKED.name()))) {
            throw UserException.userIsBlocked(user.getId().toString());
        }
        if (Objects.equals(user.getStatus(), userStatusService.findByName(UserStatus.Name.DELETED.name()))) {
            throw UserException.userIsDeleted(user.getId().toString());
        }
    }

    private void accountFromBelongToCurrentUser (Account accountFrom, User currentUser) {
        if (!Objects.equals(accountFrom.getUser(), currentUser)) {
            throw TransactionException.accountNotBelongToCurrentUser();
        }
    }

    private Integer generateVerificationCode () {
        Random random = new Random();
        return random.nextInt(9000) + 1000;
    }

    private Credit foundActiveCredit (Long accountId) {
        List<Credit> allFoundCredit = creditService.findCreditByAccountId(accountId);
        CreditStatus activeStatus = creditStatusService.findStatusByName(CreditStatus.Name.ACTIVE.name());
        for (Credit credit : allFoundCredit) {
            if (Objects.equals(credit.getStatus(), activeStatus)) {
                return credit;
            }
        }
        throw CreditException.noActiveCredit();
    }

    private boolean equalsDate (LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return localDateTime1.getYear() == localDateTime2.getYear() &&
                localDateTime1.getMonthValue() == localDateTime2.getMonthValue() &&
                localDateTime1.getDayOfMonth() == localDateTime2.getDayOfMonth();
    }
}