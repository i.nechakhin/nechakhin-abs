package ru.ccfit.inechakhin.facade;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.telegramchat.*;

@Component
public class TelegramChatFacade {
    private final ITelegramChatService telegramChatService;
    private final IUserService userService;

    public TelegramChatFacade (ITelegramChatService telegramChatService, IUserService userService) {
        this.telegramChatService = telegramChatService;
        this.userService = userService;
    }

    public TelegramChatTokenDto generateTelegramToken (Long userId) {
        String newToken = RandomStringUtils.randomAlphanumeric(20);
        TelegramChat telegramChatWithToken = telegramChatService.findByUserId(userId);

        User userForTelegramToken = userService.findUserById(userId);
        telegramChatWithToken.setUser(userForTelegramToken);
        telegramChatWithToken.setToken(newToken);
        telegramChatService.save(telegramChatWithToken);

        return new TelegramChatTokenDto(newToken);
    }

}