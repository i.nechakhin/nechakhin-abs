package ru.ccfit.inechakhin.facade;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.mapper.RatesMapper;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.account.*;

import java.util.LinkedList;
import java.util.List;

@Service
public class RatesFacade {
    private final IRatesService ratesService;
    private final RatesMapper ratesMapper;

    public RatesFacade (IRatesService ratesService, RatesMapper ratesMapper) {
        this.ratesService = ratesService;
        this.ratesMapper = ratesMapper;
    }

    public AccountRatesDto createRates (AccountRatesDto accountCreateRatesRequest) {
        Rates createdRates;
        try {
            createdRates = ratesService.findRatesByMinMaxAmountAndMinMaxTermAndAnnualRate(
                    accountCreateRatesRequest.getMinAmount(),
                    accountCreateRatesRequest.getMaxAmount(),
                    accountCreateRatesRequest.getMinTerm(),
                    accountCreateRatesRequest.getMaxTerm(),
                    accountCreateRatesRequest.getAnnualRate()
            );
        } catch (EntityNotFoundException e) {
            Rates ratesForCreate = ratesMapper.accountRatesDtoToRates(accountCreateRatesRequest);
            createdRates = ratesService.saveRates(ratesForCreate);
        }
        return ratesMapper.ratesToAccountRatesDto(createdRates);
    }

    public Rates createAndReturnRates (AccountRatesDto accountCreateRatesRequest) {
        Rates ratesForCreate = ratesMapper.accountRatesDtoToRates(accountCreateRatesRequest);
        return ratesService.saveRates(ratesForCreate);
    }

    public AccountRatesDto updateRates (Long id, AccountRatesDto accountUpdateRatesRequest) {
        Rates ratesForUpdate = ratesService.findRatesById(id);
        ratesForUpdate = ratesMapper.update(accountUpdateRatesRequest, ratesForUpdate);
        ratesForUpdate = ratesService.saveRates(ratesForUpdate);
        return ratesMapper.ratesToAccountRatesDto(ratesForUpdate);
    }

    public AccountRatesDto findRatesById (Long id) {
        Rates foundRates = ratesService.findRatesById(id);
        return ratesMapper.ratesToAccountRatesDto(foundRates);
    }

    public List<AccountRatesDto> findAllRates (Integer offset, Integer pageSize) {
        Iterable<Rates> foundRates = ratesService.findAllRates(offset, pageSize);
        List<Rates> ratesList = new LinkedList<>();
        foundRates.forEach(ratesList::add);
        return ratesMapper.mapListRatesToListAccountRatesDto(ratesList);
    }

    public String deleteRates (Long id) {
        Rates ratesForDelete = ratesService.findRatesById(id);
        ratesService.deleteRates(ratesForDelete);
        return "Rates has been successfully delete";
    }
}
