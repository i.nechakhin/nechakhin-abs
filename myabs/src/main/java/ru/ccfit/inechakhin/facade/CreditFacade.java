package ru.ccfit.inechakhin.facade;

import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.exception.*;
import ru.ccfit.inechakhin.mapper.CreditMapper;
import ru.ccfit.inechakhin.mapper.TransactionSourceMapper;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.credit.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Service
public class CreditFacade {
    private final IUserService userService;
    private final IUserStatusService userStatusService;
    private final IAccountService accountService;
    private final IRatesService ratesService;
    private final ICreditStatusService creditStatusService;
    private final ICreditService creditService;
    private final ITransactionSourceService transactionSourceService;
    private final TransactionSourceMapper transactionSourceMapper;
    private final CreditMapper creditMapper;

    public CreditFacade (IUserService userService, IUserStatusService userStatusService, IAccountService accountService,
                         IRatesService ratesService, ICreditStatusService creditStatusService,
                         ICreditService creditService, ITransactionSourceService transactionSourceService,
                         TransactionSourceMapper transactionSourceMapper, CreditMapper creditMapper) {
        this.userService = userService;
        this.userStatusService = userStatusService;
        this.accountService = accountService;
        this.ratesService = ratesService;
        this.creditStatusService = creditStatusService;
        this.creditService = creditService;
        this.transactionSourceService = transactionSourceService;
        this.transactionSourceMapper = transactionSourceMapper;
        this.creditMapper = creditMapper;
    }

    public CreditInfoDto createCredit (Long userId, Long accountId, CreditCreateUpdateDto creditCreateRequest) {
        User user = userService.findUserById(userId);
        isUserBlockedOrDeleted(user);

        Account account = accountService.findAccountById(accountId);
        Rates rates = ratesService.findRatesById(account.getRates().getId());
        isCreditNotMatchRates(creditCreateRequest, rates);
        isAlreadyHaveActiveCredit(accountId);

        CreditStatus creditStatus = creditStatusService.findStatusByName(CreditStatus.Name.ACTIVE.name());

        Credit creditForCreate = creditMapper.creditCreateUpdateDtoToCredit(creditCreateRequest);
        creditForCreate.setAccount(account);
        creditForCreate.setStatus(creditStatus);
        Credit createdCredit = creditService.saveCredit(creditForCreate);

        accountService.addMoney(account, createdCredit.getRemainingAmount());

        return creditMapper.creditToCreditInfoDto(createdCredit);
    }

    public CreditInfoDto updateCredit (Long userId, Long accountId, Long creditId, CreditCreateUpdateDto creditUpdateRequest) {
        User user = userService.findUserById(userId);
        accountService.findAccountById(accountId);
        isUserBlockedOrDeleted(user);

        Credit creditForUpdate = creditService.findCreditById(creditId);
        creditForUpdate = creditMapper.update(creditUpdateRequest, creditForUpdate);
        creditForUpdate = creditService.saveCredit(creditForUpdate);

        return creditMapper.creditToCreditInfoDto(creditForUpdate);
    }

    public CreditInfoDto updateCreditStatus (Long userId, Long accountId, Long creditId, CreditStatusDto creditStatusUpdateRequest) {
        User user = userService.findUserById(userId);
        accountService.findAccountById(accountId);
        isUserBlockedOrDeleted(user);

        Credit creditForUpdate = creditService.findCreditById(creditId);
        CreditStatus creditStatus = creditStatusService.findStatusByName(creditStatusUpdateRequest.getStatus());
        creditForUpdate.setStatus(creditStatus);
        Credit updateCredit = creditService.saveCredit(creditForUpdate);

        return creditMapper.creditToCreditInfoDto(updateCredit);
    }

    public List<CreditInfoDto> findAllCredits (Integer offset, Integer pageSize) {
        Iterable<Credit> foundCredits = creditService.findAllCredits(offset, pageSize);
        List<Credit> creditList = new LinkedList<>();
        foundCredits.forEach(creditList::add);
        return creditMapper.mapListCreditToListCreditInfoDto(creditList);
    }

    public CreditInfoDto findCreditById (Long userId, Long accountId, Long creditId) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);
        Credit foundCredit = creditService.findCreditById(creditId);
        return creditMapper.creditToCreditInfoDto(foundCredit);
    }

    public List<CreditInfoDto> findCreditByAccountId (Long userId, Long accountId) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);
        List<Credit> allFoundCredit = creditService.findCreditByAccountId(accountId);
        return creditMapper.mapListCreditToListCreditInfoDto(allFoundCredit);
    }

    public List<CreditInfoDto> findCreditByAccountIdAndStatusId (Long userId, Long accountId, Long statusId) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);
        List<Credit> allFoundCredit = creditService.findCreditByAccountIdAndStatusId(accountId, statusId);
        return creditMapper.mapListCreditToListCreditInfoDto(allFoundCredit);
    }

    public List<CreditMonthlyInfoDto> drawUpPaymentSchedule (Long userId, Long accountId) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Credit activeCredit = null;
        List<Credit> allFoundCredit = creditService.findCreditByAccountId(accountId);
        CreditStatus activeStatus = creditStatusService.findStatusByName(CreditStatus.Name.ACTIVE.name());
        for (Credit credit : allFoundCredit) {
            if (Objects.equals(credit.getStatus(), activeStatus)) {
                activeCredit = credit;
            }
        }
        if (activeCredit == null) {
            throw CreditException.noActiveCredit();
        }

        List<CreditMonthlyInfoDto> creditMonthlyInfoList = new LinkedList<>();
        LocalDateTime datePayment = creditService.getActualPaymentDate(activeCredit);
        BigDecimal paymentAmount = creditService.getMonthlyPayment(activeCredit);
        BigDecimal interestPaymentAmount = creditService.getMonthlyOverpayment(activeCredit);
        BigDecimal mainPaymentAmount = paymentAmount.subtract(interestPaymentAmount);
        BigDecimal remainingAmount = activeCredit.getRemainingAmount();
        BigDecimal monthlyAnnualRate = creditService.getMonthlyAnnualRate(activeCredit);
        for (int i = 0; i < activeCredit.getRemainingTerm().intValue(); ++i) {
            CreditMonthlyInfoDto creditMonthlyInfoDto = new CreditMonthlyInfoDto(datePayment,
                    paymentAmount, mainPaymentAmount, interestPaymentAmount, remainingAmount);
            creditMonthlyInfoList.add(creditMonthlyInfoDto);
            if (datePayment.getMonthValue() == 12) {
                datePayment = LocalDateTime.of(datePayment.getYear() + 1, 1, datePayment.getDayOfMonth(), 0, 0);
            } else {
                datePayment = LocalDateTime.of(datePayment.getYear(), datePayment.getMonthValue() + 1, datePayment.getDayOfMonth(), 0, 0);
            }
            remainingAmount = remainingAmount.subtract(mainPaymentAmount);
            interestPaymentAmount = remainingAmount.multiply(monthlyAnnualRate);
            mainPaymentAmount = paymentAmount.subtract(interestPaymentAmount);
        }
        return creditMonthlyInfoList;
    }

    public CreditFullInfoDto getFullInfoAboutActiveCredit (Long userId, Long accountId) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);
        Credit activeCredit = foundActiveCredit(accountId);

        CreditFullInfoDto creditFullInfoDto = new CreditFullInfoDto();
        creditFullInfoDto.setRemainingAmount(activeCredit.getRemainingAmount());
        creditFullInfoDto.setRemainingTerm(activeCredit.getRemainingTerm());
        creditFullInfoDto.setAnnualRate(activeCredit.getAnnualRate());
        creditFullInfoDto.setPaymentForRepaymentCredit(creditService.getPaymentForRepaymentCredit(activeCredit));
        creditFullInfoDto.setDailyFine(BigDecimal.valueOf(5.0));
        creditFullInfoDto.setAllSource(transactionSourceMapper.mapListTransactionSourceToListTransactionSourceDto(transactionSourceService.findAll()));

        return creditFullInfoDto;
    }

    private void isUserBlockedOrDeleted (User user) {
        if (Objects.equals(user.getStatus(), userStatusService.findByName(UserStatus.Name.BLOCKED.name()))) {
            throw UserException.userIsBlocked(user.getId().toString());
        }
        if (Objects.equals(user.getStatus(), userStatusService.findByName(UserStatus.Name.DELETED.name()))) {
            throw UserException.userIsDeleted(user.getId().toString());
        }
    }

    private void isCreditNotMatchRates (CreditCreateUpdateDto credit, Rates rates) {
        if (rates.getMinAmount().compareTo(credit.getRemainingAmount()) > 0 ||
                rates.getMaxAmount().compareTo(credit.getRemainingAmount()) < 0 ||
                rates.getMinTerm().compareTo(credit.getRemainingTerm()) > 0 ||
                rates.getMaxTerm().compareTo(credit.getRemainingTerm()) < 0 ||
                rates.getAnnualRate().compareTo(credit.getAnnualRate()) != 0) {
            throw CreditException.creditNotMatchRates();
        }
    }

    private void isAlreadyHaveActiveCredit (Long accountId) {
        List<Credit> allFoundCredit = creditService.findCreditByAccountId(accountId);
        CreditStatus closedStatus = creditStatusService.findStatusByName(CreditStatus.Name.CLOSED.name());
        for (Credit credit : allFoundCredit) {
            if (!Objects.equals(credit.getStatus(), closedStatus)) {
                throw CreditException.alreadyHaveActiveCredit(credit.getId().toString());
            }
        }
    }

    private Credit foundActiveCredit (Long accountId) {
        List<Credit> allFoundCredit = creditService.findCreditByAccountId(accountId);
        CreditStatus activeStatus = creditStatusService.findStatusByName(CreditStatus.Name.ACTIVE.name());
        for (Credit credit : allFoundCredit) {
            if (Objects.equals(credit.getStatus(), activeStatus)) {
                return credit;
            }
        }
        throw CreditException.noActiveCredit();
    }
}
