package ru.ccfit.inechakhin.facade;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.service.impl.UserCredentialServiceImpl;
import ru.ccfit.inechakhin.web.dto.auth.*;
import ru.ccfit.inechakhin.web.security.jwt.JwtUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AuthFacade {
    private final UserCredentialServiceImpl userCredentialService;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    @Value("${server.port}")
    private int portNumber;

    public AuthFacade (UserCredentialServiceImpl userCredentialService, AuthenticationManager authenticationManager,
                       PasswordEncoder encoder, JwtUtils jwtUtils) {
        this.userCredentialService = userCredentialService;
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    public LoginResponse loginUser (LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = getNewToken(authentication);

        UserCredential userDetails = (UserCredential) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new LoginResponse(jwt, userDetails.getId(), userDetails.getUsername(), roles);
    }

    private String getNewToken (Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtUtils.generateJwtToken(authentication);
    }

    public void logoutUser (String jwt) {
        JwtUtils.addToBlacklist(jwt);
    }

    public String startPasswordRecovery (LoginRequest loginRequest) {
        String token = RandomStringUtils.randomAlphabetic(20);

        UserCredential userCredential = (UserCredential) userCredentialService.loadUserByUsername(loginRequest.getUsername());
        userCredentialService.setPassword(userCredential, token);

        return "localhost:" + portNumber + "/auth/endPasswordRecovery?token=" + token;
    }

    public String endPasswordRecovery (String token, LoginRequest loginRequest) {
        UserCredential userCredential = (UserCredential) userCredentialService.loadUserByUsername(loginRequest.getUsername());
        if (Objects.equals(loginRequest.getUsername(), userCredential.getUsername()) &&
                Objects.equals(token, userCredential.getPassword())) {
            userCredentialService.setPassword(userCredential, encoder.encode(loginRequest.getPassword()));
        }
        return "Password has been successfully changed";
    }
}
