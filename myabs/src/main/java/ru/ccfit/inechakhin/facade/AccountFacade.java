package ru.ccfit.inechakhin.facade;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.mapper.AccountMapper;
import ru.ccfit.inechakhin.mapper.TransactionMapper;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.account.*;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionInfoDto;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Service
public class AccountFacade {
    private final IUserService userService;
    private final IAccountService accountService;
    private final IAccountStatusService accountStatusService;
    private final IRatesService ratesService;
    private final ITransactionService transactionService;
    private final ICreditService creditService;
    private final ICreditStatusService creditStatusService;
    private final RatesFacade ratesFacade;
    private final TransactionMapper transactionMapper;
    private final AccountMapper accountMapper;

    public AccountFacade (IUserService userService, IAccountService accountService, IAccountStatusService accountStatusService,
                          IRatesService ratesService, ITransactionService transactionService, ICreditService creditService,
                          ICreditStatusService creditStatusService, RatesFacade ratesFacade,
                          TransactionMapper transactionMapper, AccountMapper accountMapper) {
        this.userService = userService;
        this.accountService = accountService;
        this.accountStatusService = accountStatusService;
        this.ratesService = ratesService;
        this.transactionService = transactionService;
        this.creditService = creditService;
        this.creditStatusService = creditStatusService;
        this.ratesFacade = ratesFacade;
        this.transactionMapper = transactionMapper;
        this.accountMapper = accountMapper;
    }

    public AccountInfoDto createAccount (Long userId, AccountCreateDto accountCreateDto) {
        User user = userService.findUserById(userId);
        AccountStatus status = accountStatusService.findByName(AccountStatus.Name.CREATED.name());

        Account accountForCreate = new Account();
        accountForCreate.setUser(user);
        accountForCreate.setStatus(status);
        Account createdAccount = accountService.saveAccount(accountForCreate);

        return accountMapper.accountToAccountInfoDto(createdAccount);
    }

    public List<AccountInfoDto> findAllAccounts (Integer offset, Integer pageSize) {
        Iterable<Account> findAccounts = accountService.findAllAccounts(offset, pageSize);
        List<Account> accountList = new LinkedList<>();
        findAccounts.forEach(accountList::add);
        return accountMapper.mapListAccountToListAccountInfoDto(accountList);
    }

    public AccountInfoDto findAccountById (Long userId, Long accountId) {
        userService.findUserById(userId);
        Account foundAccount = accountService.findAccountById(accountId);
        return accountMapper.accountToAccountInfoDto(foundAccount);
    }

    public AccountInfoDto changeAccountStatus (Long userId, Long accountId, AccountStatusDto accountUpdateRequest) {
        userService.findUserById(userId);

        Account accountForUpdate = accountService.findAccountById(accountId);
        AccountStatus status = accountStatusService.findByName(accountUpdateRequest.getStatusName());
        accountForUpdate.setStatus(status);
        Account updatedAccount = accountService.saveAccount(accountForUpdate);
        return accountMapper.accountToAccountInfoDto(updatedAccount);
    }

    public AccountInfoDto changeAccountRates (Long userId, Long accountId, AccountRatesDto accountUpdateRequest) {
        userService.findUserById(userId);

        Account accountForUpdate = accountService.findAccountById(accountId);
        Rates rates;
        try {
            rates = ratesService.findRatesByMinMaxAmountAndMinMaxTermAndAnnualRate(
                    accountUpdateRequest.getMinAmount(),
                    accountUpdateRequest.getMaxAmount(),
                    accountUpdateRequest.getMinTerm(),
                    accountUpdateRequest.getMaxTerm(),
                    accountUpdateRequest.getAnnualRate()
            );
        } catch (EntityNotFoundException e) {
            rates = ratesFacade.createAndReturnRates(accountUpdateRequest);
        }
        accountForUpdate.setRates(rates);
        Account updatedAccount = accountService.saveAccount(accountForUpdate);
        return accountMapper.accountToAccountInfoDto(updatedAccount);
    }

    public List<AccountInfoDto> findAccountsByUserId (Long id) {
        userService.findUserById(id);
        List<Account> foundAccounts = accountService.findAccountsByUserId(id);
        return accountMapper.mapListAccountToListAccountInfoDto(foundAccounts);
    }

    public List<AccountInfoDto> findAccountsByUserIdAndStatusId (Long userId, Long statusId) {
        userService.findUserById(userId);
        List<Account> foundAccounts = accountService.findAccountsByUserIdAndStatusId(userId, statusId);
        return accountMapper.mapListAccountToListAccountInfoDto(foundAccounts);
    }

    public List<TransactionInfoDto> findAllTransactionByAccountId (Long userId, Long accountId, Integer offset, Integer pageSize) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Iterable<Transaction> allFoundAccount = transactionService.findAllTransactionByAccountId(accountId, offset, pageSize);
        List<Transaction> transactionList = new LinkedList<>();
        allFoundAccount.forEach(transactionList::add);
        return transactionMapper.mapListTransactionToListTransactionInfoDto(transactionList);
    }

    public List<TransactionInfoDto> findAllTransactionByAccountIdAndStatusId (Long userId, Long accountId, Long statusId, Integer offset, Integer pageSize) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Iterable<Transaction> allFoundAccount = transactionService.findAllTransactionByAccountIdAndStatusId(accountId, statusId, offset, pageSize);
        List<Transaction> transactionList = new LinkedList<>();
        allFoundAccount.forEach(transactionList::add);
        return transactionMapper.mapListTransactionToListTransactionInfoDto(transactionList);
    }

    public List<TransactionInfoDto> findAllTransactionByAccountIdAndTypeId (Long userId, Long accountId, Long typeId, Integer offset, Integer pageSize) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Iterable<Transaction> allFoundAccount = transactionService.findAllTransactionByAccountIdAndTypeId(accountId, typeId, offset, pageSize);
        List<Transaction> transactionList = new LinkedList<>();
        allFoundAccount.forEach(transactionList::add);
        return transactionMapper.mapListTransactionToListTransactionInfoDto(transactionList);
    }

    public List<TransactionInfoDto> findAllTransactionByAccountIdAndSourceFromId (Long userId, Long accountId, Long sourceId, Integer offset, Integer pageSize) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Iterable<Transaction> allFoundAccount = transactionService.findAllTransactionByAccountIdAndSourceFromId(accountId, sourceId, offset, pageSize);
        List<Transaction> transactionList = new LinkedList<>();
        allFoundAccount.forEach(transactionList::add);
        return transactionMapper.mapListTransactionToListTransactionInfoDto(transactionList);
    }

    public List<TransactionInfoDto> findAllTransactionByAccountIdAndSourceToId (Long userId, Long accountId, Long sourceId, Integer offset, Integer pageSize) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Iterable<Transaction> allFoundAccount = transactionService.findAllTransactionByAccountIdAndSourceToId(accountId, sourceId, offset, pageSize);
        List<Transaction> transactionList = new LinkedList<>();
        allFoundAccount.forEach(transactionList::add);
        return transactionMapper.mapListTransactionToListTransactionInfoDto(transactionList);
    }

    public List<AccountInfoDto> findAllAccountsWithoutRates (Long userId) {
        userService.findUserById(userId);
        List<Account> allFoundAccounts = accountService.findAccountsByUserId(userId);
        return allFoundAccounts.stream()
                .filter(account -> account.getRates() == null)
                .map(accountMapper::accountToAccountInfoDto)
                .toList();
    }

    public List<AccountInfoDto> findAllAccountsWithRatesWithoutActiveCredit (Long userId) {
        userService.findUserById(userId);
        List<Account> allFoundAccounts = accountService.findAccountsByUserId(userId);
        return allFoundAccounts.stream()
                .filter(account -> account.getRates() != null)
                .filter(account -> !isActiveCreditExist(account.getId()))
                .map(accountMapper::accountToAccountInfoDto)
                .toList();
    }

    public List<AccountInfoDto> findAllAccountsWithActiveCredit (Long userId) {
        userService.findUserById(userId);
        List<Account> allFoundAccounts = accountService.findAccountsByUserId(userId);
        return allFoundAccounts.stream()
                .filter(account -> isActiveCreditExist(account.getId()))
                .map(accountMapper::accountToAccountInfoDto)
                .toList();
    }

    private boolean isActiveCreditExist (Long accountId) {
        List<Credit> allFoundCredit = creditService.findCreditByAccountId(accountId);
        CreditStatus activeStatus = creditStatusService.findStatusByName(CreditStatus.Name.ACTIVE.name());
        for (Credit credit : allFoundCredit) {
            if (Objects.equals(credit.getStatus(), activeStatus)) {
                return true;
            }
        }
        return false;
    }
}