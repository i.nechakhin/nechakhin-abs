package ru.ccfit.inechakhin.facade;

import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.exception.UserException;
import ru.ccfit.inechakhin.mapper.CardMapper;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.card.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Service
public class CardFacade {
    private final IUserService userService;
    private final IUserStatusService userStatusService;
    private final IAccountService accountService;
    private final ICardService cardService;
    private final ICardStatusService cardStatusService;
    private final ICardPaymentSystemService cardPaymentSystemService;
    private final CardMapper cardMapper;

    public CardFacade (IUserService userService, IUserStatusService userStatusService, IAccountService accountService, ICardService cardService, ICardStatusService cardStatusService,
                       ICardPaymentSystemService cardPaymentSystemService, CardMapper cardMapper) {
        this.userService = userService;
        this.userStatusService = userStatusService;
        this.accountService = accountService;
        this.cardService = cardService;
        this.cardStatusService = cardStatusService;
        this.cardPaymentSystemService = cardPaymentSystemService;
        this.cardMapper = cardMapper;
    }

    public CardInfoDto createCard (Long userId, Long accountId, CardCreateDto cardCreateRequest) {
        User user = userService.findUserById(userId);
        isUserBlockedOrDeleted(user);

        CardStatus cardStatus = cardStatusService.findStatusByName(CardStatus.Name.ACTIVE.name());
        Account account = accountService.findAccountById(accountId);
        CardPaymentSystem cardPaymentSystem = cardPaymentSystemService.findCardPaymentSystemByName(cardCreateRequest.getPaymentSystem());

        Card cardForCreate = new Card();
        cardForCreate.setNumber(cardCreateRequest.getNumber());
        cardForCreate.setCvv(cardCreateRequest.getCvv());
        cardForCreate.setExpDate(cardCreateRequest.getExpDate());
        cardForCreate.setStatus(cardStatus);
        cardForCreate.setAccount(account);
        cardForCreate.setPaymentSystem(cardPaymentSystem);
        Card createdCard = cardService.saveCard(cardForCreate);

        return cardMapper.cardToCardInfoDto(createdCard);
    }

    public List<CardInfoDto> findAllCards (Integer offset, Integer pageSize) {
        Iterable<Card> foundCards = cardService.findAllCard(offset, pageSize);
        List<Card> cardList = new LinkedList<>();
        foundCards.forEach(cardList::add);
        return cardMapper.mapListCardToListCardInfoDto(cardList);
    }

    public CardInfoDto findCardById (Long userId, Long accountId, Long cardId) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Card foundCard = cardService.findCardById(cardId);
        return cardMapper.cardToCardInfoDto(foundCard);
    }

    public CardInfoDto findCardByNumber (Long userId, Long accountId, Long number) {
        userService.findUserById(userId);
        accountService.findAccountById(accountId);

        Card foundCard = cardService.findCardByNumber(number);
        return cardMapper.cardToCardInfoDto(foundCard);
    }

    public CardInfoDto updateStatus (Long userId, Long accountId, Long number, CardStatusDto cardStatusUpdateRequest) {
        User user = userService.findUserById(userId);
        accountService.findAccountById(accountId);
        isUserBlockedOrDeleted(user);

        Card cardForUpdate = cardService.findCardByNumber(number);
        CardStatus cardStatus = cardStatusService.findStatusByName(cardStatusUpdateRequest.getStatus());
        cardForUpdate.setStatus(cardStatus);
        Card updatedCard = cardService.saveCard(cardForUpdate);

        return cardMapper.cardToCardInfoDto(updatedCard);
    }

    private void isUserBlockedOrDeleted (User user) {
        if (Objects.equals(user.getStatus(), userStatusService.findByName(UserStatus.Name.BLOCKED.name()))) {
            throw UserException.userIsBlocked(user.getId().toString());
        }
        if (Objects.equals(user.getStatus(), userStatusService.findByName(UserStatus.Name.DELETED.name()))) {
            throw UserException.userIsDeleted(user.getId().toString());
        }
    }
}