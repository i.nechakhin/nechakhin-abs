package ru.ccfit.inechakhin.facade;

import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.mapper.*;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.account.AccountInfoDto;
import ru.ccfit.inechakhin.web.dto.user.*;

import java.util.LinkedList;
import java.util.List;

@Service
public class UserFacade {
    private final IUserService userService;
    private final IRoleService roleService;
    private final IAccountService accountService;
    private final IUserStatusService userStatusService;
    private final PasswordEncoder encoder;
    private final UserMapper userMapper;
    private final AccountMapper accountMapper;

    public UserFacade (IUserService userService, IRoleService roleService, IAccountService accountService,
                       IUserStatusService userStatusService, PasswordEncoder encoder,
                       UserMapper userMapper, AccountMapper accountMapper) {
        this.userService = userService;
        this.roleService = roleService;
        this.accountService = accountService;
        this.userStatusService = userStatusService;
        this.encoder = encoder;
        this.userMapper = userMapper;
        this.accountMapper = accountMapper;
    }

    public UserInfoDto findUserById (Long id) {
        User foundUser = userService.findUserById(id);
        return userMapper.userToUserInfoDto(foundUser);
    }

    public List<UserInfoDto> findAllUsers (Integer offset, Integer pageSize) {
        Iterable<User> findUsers = userService.findAllUsers(offset, pageSize);
        List<User> userList = new LinkedList<>();
        findUsers.forEach(userList::add);
        return userMapper.mapListUserToListUserInfoDto(userList);
    }

    @Transactional
    public UserInfoDto createUser (UserCreateUpdateDto userCreateRequest) {
        UserStatus userStatus = userStatusService.findByName(UserStatus.Name.ACTIVE.name());
        Role role = roleService.findRoleByName(Role.Name.USER.name());
        TelegramChat telegramChat = new TelegramChat();
        UserCredential userCredential = new UserCredential();

        userCredential.setUsername(userCreateRequest.getUserCredentialDto().getUsername());
        userCredential.setPassword(encoder.encode(userCreateRequest.getUserCredentialDto().getPassword()));

        User createdUser = userMapper.userCreateUpdateDtoToUser(userCreateRequest);
        createdUser.setStatus(userStatus);
        createdUser.setTelegramChat(telegramChat);
        createdUser.setCredential(userCredential);
        createdUser.setRoles(List.of(role));
        createdUser = userService.saveUser(createdUser);

        return userMapper.userToUserInfoDto(createdUser);
    }

    public UserInfoDto updateUser (Long id, UserCreateUpdateDto userUpdateRequest) {
        User userForUpdate = userService.findUserById(id);
        userForUpdate = userMapper.update(userUpdateRequest, userForUpdate);
        userForUpdate = userService.saveUser(userForUpdate);
        return userMapper.userToUserInfoDto(userForUpdate);
    }

    public boolean deleteUserById (Long id) {
        User userDelete = userService.findUserById(id);
        userDelete.setStatus(userStatusService.findByName(UserStatus.Name.DELETED.name()));
        userDelete = userService.saveUser(userDelete);
        return userDelete.getStatus().getName().equals(UserStatus.Name.DELETED.name());
    }

    public UserInfoDto updateUserStatus (Long userId, UserStatusDto userStatusRequest) {
        User userForChangeStatus = userService.findUserById(userId);
        UserStatus userStatus = userStatusService.findByName(userStatusRequest.getStatus());
        userForChangeStatus.setStatus(userStatus);
        User updatedUser = userService.saveUser(userForChangeStatus);
        return userMapper.userToUserInfoDto(updatedUser);
    }

    public List<AccountInfoDto> findAllUserAccounts (Long userId) {
        List<Account> foundAccounts = accountService.findAccountsByUserId(userId);
        return accountMapper.mapListAccountToListAccountInfoDto(foundAccounts);
    }
}
