package ru.ccfit.inechakhin.facade;

import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.database.entity.*;
import ru.ccfit.inechakhin.mapper.BlockingMapper;
import ru.ccfit.inechakhin.service.*;
import ru.ccfit.inechakhin.web.dto.blocking.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class BlockingFacade {
    private final IUserService userService;
    private final IUserStatusService userStatusService;
    private final IAccountService accountService;
    private final IAccountStatusService accountStatusService;
    private final IBlockingService blockingService;
    private final BlockingMapper blockingMapper;

    public BlockingFacade (IUserService userService, IUserStatusService userStatusService, IAccountService accountService,
                           IAccountStatusService accountStatusService, IBlockingService blockingService, BlockingMapper blockingMapper) {
        this.userService = userService;
        this.userStatusService = userStatusService;
        this.accountService = accountService;
        this.accountStatusService = accountStatusService;
        this.blockingService = blockingService;
        this.blockingMapper = blockingMapper;
    }

    public BlockingInfoDto createBlocking (Long userId, Long accountId, BlockingCreateDto blockingCreateRequest) {
        User user = userService.findUserById(userId);
        UserStatus userStatus = userStatusService.findByName(UserStatus.Name.BLOCKED.name());
        user.setStatus(userStatus);
        userService.saveUser(user);

        Account account = accountService.findAccountById(accountId);
        AccountStatus accountStatus = accountStatusService.findByName(AccountStatus.Name.BLOCKED.name());
        account.setStatus(accountStatus);
        accountService.saveAccount(account);

        Blocking blockingForCreate = blockingMapper.blockingCreateDtoToBlocking(blockingCreateRequest);
        blockingForCreate.setAccount(account);
        Blocking createdBlocking = blockingService.saveBlocking(blockingForCreate);

        return blockingMapper.blockingToBlockingInfoDto(createdBlocking);
    }

    public List<BlockingInfoDto> findBlockingByAccountId (Long userId, Long accountId) {
        userService.findUserById(userId);
        List<Blocking> blockingList = blockingService.findBlockingByAccountId(accountId);
        return blockingMapper.listBlockingToListBlockingInfoDto(blockingList);
    }
}
