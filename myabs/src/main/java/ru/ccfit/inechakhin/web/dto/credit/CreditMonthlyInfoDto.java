package ru.ccfit.inechakhin.web.dto.credit;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CreditMonthlyInfoDto {
    private LocalDateTime datePayment;
    private BigDecimal paymentAmount;
    private BigDecimal mainPaymentAmount;
    private BigDecimal interestPaymentAmount;
    private BigDecimal remainingAmount;

    public CreditMonthlyInfoDto () {
    }

    public CreditMonthlyInfoDto (LocalDateTime datePayment, BigDecimal paymentAmount, BigDecimal mainPaymentAmount,
                                 BigDecimal interestPaymentAmount, BigDecimal remainingAmount) {
        this.datePayment = datePayment;
        this.paymentAmount = paymentAmount;
        this.mainPaymentAmount = mainPaymentAmount;
        this.interestPaymentAmount = interestPaymentAmount;
        this.remainingAmount = remainingAmount;
    }

    public LocalDateTime getDatePayment () {
        return datePayment;
    }

    public void setDatePayment (LocalDateTime datePayment) {
        this.datePayment = datePayment;
    }

    public BigDecimal getPaymentAmount () {
        return paymentAmount;
    }

    public void setPaymentAmount (BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public BigDecimal getMainPaymentAmount () {
        return mainPaymentAmount;
    }

    public void setMainPaymentAmount (BigDecimal mainPaymentAmount) {
        this.mainPaymentAmount = mainPaymentAmount;
    }

    public BigDecimal getInterestPaymentAmount () {
        return interestPaymentAmount;
    }

    public void setInterestPaymentAmount (BigDecimal interestPaymentAmount) {
        this.interestPaymentAmount = interestPaymentAmount;
    }

    public BigDecimal getRemainingAmount () {
        return remainingAmount;
    }

    public void setRemainingAmount (BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditMonthlyInfoDto that = (CreditMonthlyInfoDto) o;
        if (!datePayment.equals(that.datePayment)) return false;
        if (!paymentAmount.equals(that.paymentAmount)) return false;
        if (!mainPaymentAmount.equals(that.mainPaymentAmount)) return false;
        if (!interestPaymentAmount.equals(that.interestPaymentAmount)) return false;
        return remainingAmount.equals(that.remainingAmount);
    }

    @Override
    public int hashCode () {
        int result = datePayment.hashCode();
        result = 31 * result + paymentAmount.hashCode();
        result = 31 * result + mainPaymentAmount.hashCode();
        result = 31 * result + interestPaymentAmount.hashCode();
        result = 31 * result + remainingAmount.hashCode();
        return result;
    }

    @Override
    public String toString () {
        return "CreditMonthlyInfoDto{" +
                "datePayment=" + datePayment +
                ", paymentAmount=" + paymentAmount +
                ", mainPaymentAmount=" + mainPaymentAmount +
                ", interestPaymentAmount=" + interestPaymentAmount +
                ", remainingAmount=" + remainingAmount +
                '}';
    }
}
