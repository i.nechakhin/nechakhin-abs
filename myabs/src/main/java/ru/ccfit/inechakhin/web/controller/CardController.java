package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.CardFacade;
import ru.ccfit.inechakhin.web.dto.card.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/account/{accountId}/cards")
public class CardController {
    private final CardFacade cardFacade;

    public CardController (CardFacade cardFacade) {
        this.cardFacade = cardFacade;
    }

    @PostMapping()
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<CardInfoDto> createCard (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @RequestBody CardCreateDto cardCreateRequest) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(cardFacade.createCard(userId, accountId, cardCreateRequest));
    }

    @GetMapping("/all/{offset}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<List<CardInfoDto>> findAllCards (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(cardFacade.findAllCards(offset, pageSize));
    }

    @GetMapping("/{cardId}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<CardInfoDto> findCardById (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long cardId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(cardFacade.findCardById(userId, accountId, cardId));
    }

    @GetMapping("/number/{cardNumber}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<CardInfoDto> findCardByNumber (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long cardNumber) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(cardFacade.findCardByNumber(userId, accountId, cardNumber));
    }

    @PatchMapping("/number/{cardNumber}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<CardInfoDto> changeCardStatus (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long cardNumber,
            @RequestBody CardStatusDto cardStatusRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(cardFacade.updateStatus(userId, accountId, cardNumber, cardStatusRequest));
    }
}