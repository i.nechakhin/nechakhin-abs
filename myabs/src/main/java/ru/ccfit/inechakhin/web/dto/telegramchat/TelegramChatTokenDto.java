package ru.ccfit.inechakhin.web.dto.telegramchat;

public class TelegramChatTokenDto {
    private String token;

    public TelegramChatTokenDto () {
    }

    public TelegramChatTokenDto (String token) {
        this.token = token;
    }

    public String getToken () {
        return token;
    }

    public void setToken (String token) {
        this.token = token;
    }
}
