package ru.ccfit.inechakhin.web.dto.card;

import java.util.Objects;

public class CardStatusDto {
    private String status;

    public CardStatusDto () {
    }

    public CardStatusDto (String status) {
        this.status = status;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardStatusDto that = (CardStatusDto) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode () {
        return Objects.hash(status);
    }

    @Override
    public String toString () {
        return "CardUpdateStatusDto{" +
                "status='" + status + '\'' +
                '}';
    }
}
