package ru.ccfit.inechakhin.web.dto.transaction;

import java.math.BigDecimal;

public class TransactionInfoDto {
    private BigDecimal amount;
    private Long accountFromId;
    private Long accountToId;
    private Long sourceFromId;
    private Long sourceToId;
    private String status;
    private String type;

    public TransactionInfoDto () {
    }

    public TransactionInfoDto (BigDecimal amount, Long accountFromId, Long accountToId, Long sourceFromId, Long sourceToId, String status, String type) {
        this.amount = amount;
        this.accountFromId = accountFromId;
        this.accountToId = accountToId;
        this.sourceFromId = sourceFromId;
        this.sourceToId = sourceToId;
        this.status = status;
        this.type = type;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public Long getAccountFromId () {
        return accountFromId;
    }

    public void setAccountFromId (Long accountFromId) {
        this.accountFromId = accountFromId;
    }

    public Long getAccountToId () {
        return accountToId;
    }

    public void setAccountToId (Long accountToId) {
        this.accountToId = accountToId;
    }

    public Long getSourceFromId () {
        return sourceFromId;
    }

    public void setSourceFromId (Long sourceFromId) {
        this.sourceFromId = sourceFromId;
    }

    public Long getSourceToId () {
        return sourceToId;
    }

    public void setSourceToId (Long sourceToId) {
        this.sourceToId = sourceToId;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }
}
