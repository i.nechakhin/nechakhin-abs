package ru.ccfit.inechakhin.web.dto.auth;

import java.util.List;

public class LoginResponse {
    private String type = "Bearer";
    private String jwt;
    private Long id;
    private String username;
    private List<String> roles;

    public LoginResponse () {
    }

    public LoginResponse (String jwt, Long id, String username, List<String> roles) {
        this.jwt = jwt;
        this.id = id;
        this.username = username;
        this.roles = roles;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getJwt () {
        return jwt;
    }

    public void setJwt (String jwt) {
        this.jwt = jwt;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getUsername () {
        return username;
    }

    public void setUsername (String username) {
        this.username = username;
    }

    public List<String> getRoles () {
        return roles;
    }

    public void setRoles (List<String> roles) {
        this.roles = roles;
    }
}
