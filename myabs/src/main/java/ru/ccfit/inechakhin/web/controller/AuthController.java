package ru.ccfit.inechakhin.web.controller;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.AuthFacade;
import ru.ccfit.inechakhin.web.dto.auth.*;
import ru.ccfit.inechakhin.web.security.jwt.JwtUtils;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthFacade authFacade;
    private final HttpServletRequest httpServletRequest;

    public AuthController (AuthFacade authFacade, HttpServletRequest httpServletRequest) {
        this.authFacade = authFacade;
        this.httpServletRequest = httpServletRequest;
    }

    @PostMapping("/login")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<LoginResponse> login (@RequestBody LoginRequest loginRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(authFacade.loginUser(loginRequest));
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout () {
        authFacade.logoutUser(JwtUtils.parseJwt(httpServletRequest));
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("Вы успешно вышли");
    }

    @PostMapping("/passwordRecovery")
    public ResponseEntity<String> startPasswordRecovery(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(authFacade.startPasswordRecovery(loginRequest));
    }

    @PostMapping("/endPasswordRecovery")
    public ResponseEntity<String> endPasswordRecovery (
            @RequestParam String token,
            @RequestBody LoginRequest loginRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(authFacade.endPasswordRecovery(token, loginRequest));
    }
}