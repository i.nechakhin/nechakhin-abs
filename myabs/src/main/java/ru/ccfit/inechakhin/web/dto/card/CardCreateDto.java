package ru.ccfit.inechakhin.web.dto.card;

import java.time.LocalDate;

public class CardCreateDto {
    private Long number;
    private LocalDate expDate;
    private Integer cvv;
    private String paymentSystem;

    public CardCreateDto () {
    }

    public CardCreateDto (Long number, LocalDate expDate, Integer cvv, String paymentSystem) {
        this.number = number;
        this.expDate = expDate;
        this.cvv = cvv;
        this.paymentSystem = paymentSystem;
    }

    public Long getNumber () {
        return number;
    }

    public void setNumber (Long number) {
        this.number = number;
    }

    public LocalDate getExpDate () {
        return expDate;
    }

    public void setExpDate (LocalDate expDate) {
        this.expDate = expDate;
    }

    public Integer getCvv () {
        return cvv;
    }

    public void setCvv (Integer cvv) {
        this.cvv = cvv;
    }

    public String getPaymentSystem () {
        return paymentSystem;
    }

    public void setPaymentSystem (String paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    @Override
    public String toString () {
        return "CardCreateDto{" +
                "number=" + number +
                ", expDate=" + expDate +
                ", cvv=" + cvv +
                ", paymentSystem='" + paymentSystem + '\'' +
                '}';
    }
}
