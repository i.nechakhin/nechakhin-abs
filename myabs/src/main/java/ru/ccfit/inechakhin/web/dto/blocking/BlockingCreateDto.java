package ru.ccfit.inechakhin.web.dto.blocking;

import java.time.LocalDateTime;

public class BlockingCreateDto {
    private LocalDateTime startBlock;
    private LocalDateTime endBlock;

    public BlockingCreateDto () {
    }

    public BlockingCreateDto (LocalDateTime startBlock, LocalDateTime endBlock) {
        this.startBlock = startBlock;
        this.endBlock = endBlock;
    }

    public LocalDateTime getStartBlock () {
        return startBlock;
    }

    public void setStartBlock (LocalDateTime startBlock) {
        this.startBlock = startBlock;
    }

    public LocalDateTime getEndBlock () {
        return endBlock;
    }

    public void setEndBlock (LocalDateTime endBlock) {
        this.endBlock = endBlock;
    }

    @Override
    public String toString () {
        return "BlockingCreateDto{" +
                "startBlock=" + startBlock +
                ", endBlock=" + endBlock +
                '}';
    }
}
