package ru.ccfit.inechakhin.web.dto.account;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AccountInfoDto {
    private Long id;
    private BigDecimal amount;
    private LocalDateTime updatedAt;
    private LocalDateTime createdAt;
    private Long ratesId;
    private Long userId;
    private String statusName;

    public AccountInfoDto () {
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Long getRatesId () {
        return ratesId;
    }

    public void setRatesId (Long ratesId) {
        this.ratesId = ratesId;
    }

    public Long getUserId () {
        return userId;
    }

    public void setUserId (Long userId) {
        this.userId = userId;
    }

    public String getStatusName () {
        return statusName;
    }

    public void setStatusName (String statusName) {
        this.statusName = statusName;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountInfoDto that = (AccountInfoDto) o;
        if (!id.equals(that.id)) return false;
        if (!amount.equals(that.amount)) return false;
        if (!ratesId.equals(that.ratesId)) return false;
        return userId.equals(that.userId);
    }

    @Override
    public int hashCode () {
        int result = id.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + ratesId.hashCode();
        return result;
    }
}
