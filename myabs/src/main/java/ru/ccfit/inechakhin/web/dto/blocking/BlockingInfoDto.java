package ru.ccfit.inechakhin.web.dto.blocking;

import java.time.LocalDateTime;

public class BlockingInfoDto {
    private LocalDateTime startBlock;
    private LocalDateTime endBlock;
    private Long accountId;

    public BlockingInfoDto () {
    }

    public BlockingInfoDto (LocalDateTime startBlock, LocalDateTime endBlock, Long accountId) {
        this.startBlock = startBlock;
        this.endBlock = endBlock;
        this.accountId = accountId;
    }

    public LocalDateTime getStartBlock () {
        return startBlock;
    }

    public void setStartBlock (LocalDateTime startBlock) {
        this.startBlock = startBlock;
    }

    public LocalDateTime getEndBlock () {
        return endBlock;
    }

    public void setEndBlock (LocalDateTime endBlock) {
        this.endBlock = endBlock;
    }

    public Long getAccountId () {
        return accountId;
    }

    public void setAccountId (Long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockingInfoDto that = (BlockingInfoDto) o;
        if (!startBlock.equals(that.startBlock)) return false;
        if (!endBlock.equals(that.endBlock)) return false;
        return accountId.equals(that.accountId);
    }

    @Override
    public int hashCode () {
        int result = startBlock.hashCode();
        result = 31 * result + endBlock.hashCode();
        result = 31 * result + accountId.hashCode();
        return result;
    }

    @Override
    public String toString () {
        return "BlockingInfoDto{" +
                "startBlock=" + startBlock +
                ", endBlock=" + endBlock +
                ", accountId=" + accountId +
                '}';
    }
}
