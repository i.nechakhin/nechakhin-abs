package ru.ccfit.inechakhin.web.dto.card;

import java.time.LocalDate;
import java.util.Objects;

public class CardUpdateExpirationDateDto {
    private LocalDate expDate;

    public CardUpdateExpirationDateDto () {
    }

    public CardUpdateExpirationDateDto (LocalDate expDate) {
        this.expDate = expDate;
    }

    public LocalDate getExpDate () {
        return expDate;
    }

    public void setExpDate (LocalDate expDate) {
        this.expDate = expDate;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardUpdateExpirationDateDto that = (CardUpdateExpirationDateDto) o;
        return Objects.equals(expDate, that.expDate);
    }

    @Override
    public int hashCode () {
        return Objects.hash(expDate);
    }

    @Override
    public String toString () {
        return "CardUpdateExpirationDateDto{" +
                "expDate=" + expDate +
                '}';
    }
}
