package ru.ccfit.inechakhin.web.dto.credit;

import java.math.BigDecimal;

public class CreditCreateUpdateDto {
    private BigDecimal remainingAmount;
    private BigDecimal remainingTerm;
    private BigDecimal annualRate;

    public CreditCreateUpdateDto () {
    }

    public CreditCreateUpdateDto (BigDecimal remainingAmount, BigDecimal remainingTerm, BigDecimal annualRate) {
        this.remainingAmount = remainingAmount;
        this.remainingTerm = remainingTerm;
        this.annualRate = annualRate;
    }

    public BigDecimal getRemainingAmount () {
        return remainingAmount;
    }

    public void setRemainingAmount (BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public BigDecimal getRemainingTerm () {
        return remainingTerm;
    }

    public void setRemainingTerm (BigDecimal remainingTerm) {
        this.remainingTerm = remainingTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    @Override
    public String toString () {
        return "CreditCreateUpdateDto{" +
                "remainingAmount=" + remainingAmount +
                ", remainingTerm=" + remainingTerm +
                ", annualRate=" + annualRate +
                '}';
    }
}
