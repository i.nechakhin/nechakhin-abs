package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.AccountFacade;
import ru.ccfit.inechakhin.web.dto.account.*;
import ru.ccfit.inechakhin.web.dto.transaction.TransactionInfoDto;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/accounts")
public class AccountController {
    private final AccountFacade accountFacade;

    public AccountController (AccountFacade accountFacade) {
        this.accountFacade = accountFacade;
    }

    @GetMapping("/all/{offset}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<List<AccountInfoDto>> getAllAccounts (
            @PathVariable Long userId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllAccounts(offset, pageSize));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<AccountInfoDto>> getAllAccountsByUserId (@PathVariable Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAccountsByUserId(userId));
    }

    @GetMapping("/all/withoutRates")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<AccountInfoDto>> getAllAccountsByUserIdWithoutRates (@PathVariable Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllAccountsWithoutRates(userId));
    }

    @GetMapping("/all/withRatesAndWithoutActiveCredit")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<AccountInfoDto>> getAllAccountsByUserIdWithRatesAndWithoutActiveCredit (@PathVariable Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllAccountsWithRatesWithoutActiveCredit(userId));
    }

    @GetMapping("/all/withActiveCredit")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<AccountInfoDto>> getAllAccountsByUserIdWithActiveCredit (@PathVariable Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllAccountsWithActiveCredit(userId));
    }

    @GetMapping("/status/{statusId}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<AccountInfoDto>> getAllAccountsByUserIdAndStatusId (
            @PathVariable Long userId,
            @PathVariable Long statusId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAccountsByUserIdAndStatusId(userId, statusId));
    }

    @GetMapping("/{accountId}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<AccountInfoDto> getAccountById (
            @PathVariable Long userId,
            @PathVariable Long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAccountById(userId, accountId));
    }

    @GetMapping("/{accountId}/transaction/all/{offset}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<TransactionInfoDto>> getAllTransactionByAccountId (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllTransactionByAccountId(userId, accountId, offset, pageSize));
    }

    @GetMapping("/{accountId}/transaction/status/{statusId}/all/{offset}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<TransactionInfoDto>> getAllTransactionByAccountIdAndStatusId (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long statusId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllTransactionByAccountIdAndStatusId(userId, accountId, statusId, offset, pageSize));
    }

    @GetMapping("/{accountId}/transaction/type/{typeId}/all/{offset}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<TransactionInfoDto>> getAllTransactionByAccountIdAndTypeId (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long typeId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllTransactionByAccountIdAndTypeId(userId, accountId, typeId, offset, pageSize));
    }

    @GetMapping("/{accountId}/transaction/sourceFrom/{sourceId}/all/{offset}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<TransactionInfoDto>> getAllTransactionByAccountIdAndSourceFromId (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long sourceId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllTransactionByAccountIdAndSourceFromId(userId, accountId, sourceId, offset, pageSize));
    }

    @GetMapping("/{accountId}/transaction/sourceTo/{sourceId}/all/{offset}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<TransactionInfoDto>> getAllTransactionByAccountIdAndSourceToId (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long sourceId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.findAllTransactionByAccountIdAndSourceToId(userId, accountId, sourceId, offset, pageSize));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<AccountInfoDto> createAccount (
            @PathVariable Long userId,
            @RequestBody AccountCreateDto accountCreateRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.createAccount(userId, accountCreateRequest));
    }

    @PatchMapping("/{accountId}/status")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<AccountInfoDto> changeAccountStatus (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @RequestBody AccountStatusDto accountStatusRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.changeAccountStatus(userId, accountId, accountStatusRequest));
    }

    @PatchMapping("/{accountId}/rates")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<AccountInfoDto> changeAccountRates (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @RequestBody AccountRatesDto accountRatesRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(accountFacade.changeAccountRates(userId, accountId, accountRatesRequest));
    }
}