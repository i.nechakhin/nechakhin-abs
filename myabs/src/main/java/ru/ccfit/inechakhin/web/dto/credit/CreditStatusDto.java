package ru.ccfit.inechakhin.web.dto.credit;

public class CreditStatusDto {
    private String status;

    public CreditStatusDto () {
    }

    public CreditStatusDto (String status) {
        this.status = status;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditStatusDto that = (CreditStatusDto) o;
        return status.equals(that.status);
    }

    @Override
    public int hashCode () {
        return status.hashCode();
    }

    @Override
    public String toString () {
        return "CreditStatusDto{" +
                "status='" + status + '\'' +
                '}';
    }
}
