package ru.ccfit.inechakhin.web.dto.exchange;

import java.math.BigDecimal;

public class ExchangeInfoDto {
    private Long accountFrom;
    private Long accountTo;
    private BigDecimal amountToExchange;

    public ExchangeInfoDto () {
    }

    public ExchangeInfoDto (Long accountFrom, Long accountTo, BigDecimal amountToExchange) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amountToExchange = amountToExchange;
    }

    public Long getAccountFrom () {
        return accountFrom;
    }

    public void setAccountFrom (Long accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Long getAccountTo () {
        return accountTo;
    }

    public void setAccountTo (Long accountTo) {
        this.accountTo = accountTo;
    }

    public BigDecimal getAmountToExchange () {
        return amountToExchange;
    }

    public void setAmountToExchange (BigDecimal amountToExchange) {
        this.amountToExchange = amountToExchange;
    }
}
