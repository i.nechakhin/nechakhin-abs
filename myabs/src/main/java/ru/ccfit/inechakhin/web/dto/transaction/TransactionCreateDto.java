package ru.ccfit.inechakhin.web.dto.transaction;

import java.math.BigDecimal;

public class TransactionCreateDto {
    private BigDecimal amount;
    private Long accountFromId;
    private Long accountToId;
    private Long sourceFromId;
    private Long sourceToId;
    private Long typeId;

    public TransactionCreateDto () {
    }

    public TransactionCreateDto (BigDecimal amount, Long accountFromId, Long accountToId, Long sourceFromId, Long sourceToId, Long typeId) {
        this.amount = amount;
        this.accountFromId = accountFromId;
        this.accountToId = accountToId;
        this.sourceFromId = sourceFromId;
        this.sourceToId = sourceToId;
        this.typeId = typeId;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public Long getAccountFromId () {
        return accountFromId;
    }

    public void setAccountFromId (Long accountFromId) {
        this.accountFromId = accountFromId;
    }

    public Long getAccountToId () {
        return accountToId;
    }

    public void setAccountToId (Long accountToId) {
        this.accountToId = accountToId;
    }

    public Long getSourceFromId () {
        return sourceFromId;
    }

    public void setSourceFromId (Long sourceFromId) {
        this.sourceFromId = sourceFromId;
    }

    public Long getSourceToId () {
        return sourceToId;
    }

    public void setSourceToId (Long sourceToId) {
        this.sourceToId = sourceToId;
    }

    public Long getTypeId () {
        return typeId;
    }

    public void setTypeId (Long typeId) {
        this.typeId = typeId;
    }
}
