package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.BlockingFacade;
import ru.ccfit.inechakhin.web.dto.blocking.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/account/{accountId}/blocking")
public class BlockingController {
    BlockingFacade blockingFacade;

    public BlockingController (BlockingFacade blockingFacade) {
        this.blockingFacade = blockingFacade;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<BlockingInfoDto> createBlocking (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @RequestBody BlockingCreateDto blockingCreateDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(blockingFacade.createBlocking(userId, accountId, blockingCreateDto));
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<BlockingInfoDto>> getAllBlockingByAccountId (
            @PathVariable Long userId,
            @PathVariable Long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(blockingFacade.findBlockingByAccountId(userId, accountId));
    }
}
