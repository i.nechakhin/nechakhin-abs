package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.RatesFacade;
import ru.ccfit.inechakhin.web.dto.account.AccountRatesDto;

import java.util.List;

@RestController
@RequestMapping("/rates")
public class RatesController {
    RatesFacade ratesFacade;

    public RatesController (RatesFacade ratesFacade) {
        this.ratesFacade = ratesFacade;
    }

    @GetMapping("/all/{offset}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<List<AccountRatesDto>> getAllRates (
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ratesFacade.findAllRates(offset, pageSize));
    }

    @GetMapping("/{ratesId}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<AccountRatesDto> getRates (@PathVariable Long ratesId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ratesFacade.findRatesById(ratesId));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<AccountRatesDto> createRates (@RequestBody AccountRatesDto ratesForCreate) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ratesFacade.createRates(ratesForCreate));
    }

    @PatchMapping("/{ratesId}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<AccountRatesDto> updateRates (
            @PathVariable Long ratesId,
            @RequestBody AccountRatesDto ratesForUpdate) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ratesFacade.updateRates(ratesId, ratesForUpdate));
    }

    @DeleteMapping("/{ratesId}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<String> deleteRates (@PathVariable Long ratesId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ratesFacade.deleteRates(ratesId));
    }
}
