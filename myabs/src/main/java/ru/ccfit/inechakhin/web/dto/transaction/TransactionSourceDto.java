package ru.ccfit.inechakhin.web.dto.transaction;

import java.math.BigDecimal;

public class TransactionSourceDto {
    private String name;
    private BigDecimal commission;

    public TransactionSourceDto () {
    }

    public TransactionSourceDto (String name, BigDecimal commission) {
        this.name = name;
        this.commission = commission;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public BigDecimal getCommission () {
        return commission;
    }

    public void setCommission (BigDecimal commission) {
        this.commission = commission;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionSourceDto that = (TransactionSourceDto) o;
        if (!name.equals(that.name)) return false;
        return commission.equals(that.commission);
    }

    @Override
    public int hashCode () {
        int result = name.hashCode();
        result = 31 * result + commission.hashCode();
        return result;
    }

    @Override
    public String toString () {
        return "TransactionSourceDto{" +
                "source='" + name + '\'' +
                ", commissions=" + commission +
                '}';
    }
}
