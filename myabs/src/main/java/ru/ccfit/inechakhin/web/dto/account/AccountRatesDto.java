package ru.ccfit.inechakhin.web.dto.account;

import java.math.BigDecimal;

public class AccountRatesDto {
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private BigDecimal minTerm;
    private BigDecimal maxTerm;
    private BigDecimal annualRate;

    public AccountRatesDto () {
    }

    public AccountRatesDto (BigDecimal minAmount, BigDecimal maxAmount, BigDecimal minTerm, BigDecimal maxTerm, BigDecimal annualRate) {
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.minTerm = minTerm;
        this.maxTerm = maxTerm;
        this.annualRate = annualRate;
    }

    public BigDecimal getMinAmount () {
        return minAmount;
    }

    public void setMinAmount (BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount () {
        return maxAmount;
    }

    public void setMaxAmount (BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public BigDecimal getMinTerm () {
        return minTerm;
    }

    public void setMinTerm (BigDecimal minTerm) {
        this.minTerm = minTerm;
    }

    public BigDecimal getMaxTerm () {
        return maxTerm;
    }

    public void setMaxTerm (BigDecimal maxTerm) {
        this.maxTerm = maxTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountRatesDto that = (AccountRatesDto) o;
        if (!minAmount.equals(that.minAmount)) return false;
        if (!maxAmount.equals(that.maxAmount)) return false;
        if (!minTerm.equals(that.minTerm)) return false;
        if (!maxTerm.equals(that.maxTerm)) return false;
        return annualRate.equals(that.annualRate);
    }

    @Override
    public int hashCode () {
        int result = minAmount.hashCode();
        result = 31 * result + maxAmount.hashCode();
        result = 31 * result + minTerm.hashCode();
        result = 31 * result + maxTerm.hashCode();
        result = 31 * result + annualRate.hashCode();
        return result;
    }
}
