package ru.ccfit.inechakhin.web.dto.account;

public class AccountStatusDto {
    private String statusName;

    public AccountStatusDto () {
    }

    public AccountStatusDto (String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName () {
        return statusName;
    }

    public void setStatusName (String statusName) {
        this.statusName = statusName;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountStatusDto that = (AccountStatusDto) o;
        return statusName.equals(that.statusName);
    }

    @Override
    public int hashCode () {
        return statusName.hashCode();
    }
}
