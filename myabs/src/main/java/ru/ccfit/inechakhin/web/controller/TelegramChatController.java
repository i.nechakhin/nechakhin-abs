package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.TelegramChatFacade;
import ru.ccfit.inechakhin.web.dto.telegramchat.*;

@RestController
@RequestMapping("users/{userId}/telegram_chat")
public class TelegramChatController {
    private final TelegramChatFacade telegramChatFacade;

    public TelegramChatController (TelegramChatFacade telegramChatFacade) {
        this.telegramChatFacade = telegramChatFacade;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<TelegramChatTokenDto> getToken (@PathVariable Long userId) {
        TelegramChatTokenDto tokenResponse = telegramChatFacade.generateTelegramToken(userId);
        return ResponseEntity.ok().body(tokenResponse);
    }
}
