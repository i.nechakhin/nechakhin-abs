package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import ru.ccfit.inechakhin.facade.UserFacade;
import ru.ccfit.inechakhin.web.dto.user.*;

@RestController
@RequestMapping("/registration")
public class RegistrationController {
    UserFacade userFacade;

    public RegistrationController (UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @PostMapping
    public ResponseEntity<UserInfoDto> registration (@RequestBody UserCreateUpdateDto userCreateRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userFacade.createUser(userCreateRequest));
    }

}
