package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.TransactionFacade;
import ru.ccfit.inechakhin.web.dto.transaction.*;

@RestController
@RequestMapping("/transactions")
public class TransactionController {
    private final TransactionFacade transactionFacade;

    public TransactionController (TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<TransactionInfoDto> createTransaction (@RequestBody TransactionCreateDto transactionCreateRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(transactionFacade.createTransaction(transactionCreateRequest));
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<TransactionInfoDto> confirmTransaction (
            @PathVariable Long id,
            @RequestBody TransactionVerificationCodeDto verificationCodeRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(transactionFacade.checkVerificationCode(id, verificationCodeRequest));
    }
}
