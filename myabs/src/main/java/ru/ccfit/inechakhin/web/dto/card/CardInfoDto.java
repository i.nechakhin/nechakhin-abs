package ru.ccfit.inechakhin.web.dto.card;

import java.time.LocalDate;
import java.util.Objects;

public class CardInfoDto {
    private Long number;
    private LocalDate expDate;
    private Integer cvv;
    private Long accountId;
    private String status;
    private String paymentSystem;

    public CardInfoDto () {
    }

    public CardInfoDto (Long number, LocalDate expDate, Integer cvv, Long accountId, String status, String paymentSystem) {
        this.number = number;
        this.expDate = expDate;
        this.cvv = cvv;
        this.accountId = accountId;
        this.status = status;
        this.paymentSystem = paymentSystem;
    }

    public Long getNumber () {
        return number;
    }

    public void setNumber (Long number) {
        this.number = number;
    }

    public LocalDate getExpDate () {
        return expDate;
    }

    public void setExpDate (LocalDate expDate) {
        this.expDate = expDate;
    }

    public Integer getCvv () {
        return cvv;
    }

    public void setCvv (Integer cvv) {
        this.cvv = cvv;
    }

    public Long getAccountId () {
        return accountId;
    }

    public void setAccountId (Long accountId) {
        this.accountId = accountId;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getPaymentSystem () {
        return paymentSystem;
    }

    public void setPaymentSystem (String paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardInfoDto that = (CardInfoDto) o;
        return Objects.equals(number, that.number)
                && Objects.equals(expDate, that.expDate)
                && Objects.equals(cvv, that.cvv)
                && Objects.equals(accountId, that.accountId)
                && Objects.equals(status, that.status)
                && Objects.equals(paymentSystem, that.paymentSystem);
    }

    @Override
    public int hashCode () {
        return Objects.hash(number, expDate, cvv, accountId, status, paymentSystem);
    }

    @Override
    public String toString () {
        return "CardInfoDto{" +
                "number=" + number +
                ", expDate=" + expDate +
                ", cvv=" + cvv +
                ", accountId=" + accountId +
                ", status='" + status + '\'' +
                ", paymentSystem='" + paymentSystem + '\'' +
                '}';
    }
}
