package ru.ccfit.inechakhin.web.dto.credit;

import java.math.BigDecimal;

public class CreditInfoDto {
    private BigDecimal remainingAmount;
    private BigDecimal remainingTerm;
    private BigDecimal annualRate;
    private Long accountId;
    private String status;

    public CreditInfoDto () {
    }

    public CreditInfoDto (BigDecimal remainingAmount, BigDecimal remainingTerm, BigDecimal annualRate, Long accountId, String status) {
        this.remainingAmount = remainingAmount;
        this.remainingTerm = remainingTerm;
        this.annualRate = annualRate;
        this.accountId = accountId;
        this.status = status;
    }

    public BigDecimal getRemainingAmount () {
        return remainingAmount;
    }

    public void setRemainingAmount (BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public BigDecimal getRemainingTerm () {
        return remainingTerm;
    }

    public void setRemainingTerm (BigDecimal remainingTerm) {
        this.remainingTerm = remainingTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    public Long getAccountId () {
        return accountId;
    }

    public void setAccountId (Long accountId) {
        this.accountId = accountId;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditInfoDto that = (CreditInfoDto) o;
        if (!remainingAmount.equals(that.remainingAmount)) return false;
        if (!remainingTerm.equals(that.remainingTerm)) return false;
        if (!annualRate.equals(that.annualRate)) return false;
        if (!accountId.equals(that.accountId)) return false;
        return status.equals(that.status);
    }

    @Override
    public int hashCode () {
        int result = remainingAmount.hashCode();
        result = 31 * result + remainingTerm.hashCode();
        result = 31 * result + annualRate.hashCode();
        result = 31 * result + accountId.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    @Override
    public String toString () {
        return "CreditInfoDto{" +
                "remainingAmount=" + remainingAmount +
                ", remainingTerm=" + remainingTerm +
                ", annualRate=" + annualRate +
                ", accountId=" + accountId +
                ", status='" + status + '\'' +
                '}';
    }
}
