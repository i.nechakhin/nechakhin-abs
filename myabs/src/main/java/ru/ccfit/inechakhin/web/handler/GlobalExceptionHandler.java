package ru.ccfit.inechakhin.web.handler;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.exception.*;
import ru.ccfit.inechakhin.web.dto.error.*;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;

import static java.util.stream.Collectors.joining;

@RestControllerAdvice(annotations = RestController.class)
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorMessageDto> badRequest (MethodArgumentNotValidException exception) {
        BindingResult bindingResult = exception.getBindingResult();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ValidationErrorMessageDto(buildMessage(bindingResult), buildErrors(bindingResult)));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorMessageDto> notFound (EntityNotFoundException exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorMessageDto(exception.getMessage()));
    }

    @ExceptionHandler({AuthException.class, CreditException.class, TransactionException.class, UserException.class})
    public ResponseEntity<ErrorMessageDto> unprocessable (RuntimeException exception) {
        return ResponseEntity
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(new ErrorMessageDto(exception.getMessage()));
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorMessageDto> error (RuntimeException exception) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorMessageDto(exception.getMessage()));
    }

    private String buildMessage (BindingResult bindingResult) {
        return String.format("Error on %s, rejected errors [%s]",
                bindingResult.getTarget(),
                bindingResult.getAllErrors()
                        .stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(joining(";")));
    }

    private List<ValidationErrorMessageDto.ErrorDescription> buildErrors (BindingResult bindingResult) {
        return bindingResult.getFieldErrors()
                .stream()
                .map(e -> new ValidationErrorMessageDto.ErrorDescription(e.getField(), e.getDefaultMessage()))
                .toList();
    }
}