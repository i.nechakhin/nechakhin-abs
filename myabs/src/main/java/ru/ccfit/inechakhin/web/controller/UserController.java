package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.UserFacade;
import ru.ccfit.inechakhin.web.dto.user.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserFacade userFacade;

    public UserController (UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping("/all/{offset}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<List<UserInfoDto>> getAllUsers (
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userFacade.findAllUsers(offset, pageSize));
    }

    @GetMapping("/{userId}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<UserInfoDto> getUser (@PathVariable Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userFacade.findUserById(userId));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<UserInfoDto> createUser (@RequestBody UserCreateUpdateDto userForCreate) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userFacade.createUser(userForCreate));
    }

    @PatchMapping("/{userId}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<UserInfoDto> updateUser (
            @PathVariable Long userId,
            @RequestBody UserCreateUpdateDto userForUpdate) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userFacade.updateUser(userId, userForUpdate));
    }

    @PatchMapping("/{userId}/status")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<UserInfoDto> changeUserStatus (
            @PathVariable Long userId,
            @RequestBody UserStatusDto userStatusRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userFacade.updateUserStatus(userId, userStatusRequest));
    }
}