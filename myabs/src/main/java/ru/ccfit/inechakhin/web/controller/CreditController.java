package ru.ccfit.inechakhin.web.controller;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.ccfit.inechakhin.facade.CreditFacade;
import ru.ccfit.inechakhin.web.dto.credit.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/account/{accountId}/credits")
public class CreditController {
    CreditFacade creditFacade;

    public CreditController (CreditFacade creditFacade) {
        this.creditFacade = creditFacade;
    }

    @GetMapping("/all/{offset}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<List<CreditInfoDto>> findAllCredits (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Integer offset,
            @RequestParam Integer pageSize) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.findAllCredits(offset, pageSize));
    }

    @GetMapping("/{creditId}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<CreditInfoDto> getCredit (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long creditId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.findCreditById(userId, accountId, creditId));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<CreditInfoDto> createCredit (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @RequestBody CreditCreateUpdateDto creditCreateUpdateDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.createCredit(userId, accountId, creditCreateUpdateDto));
    }

    @PatchMapping("/{creditId}")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<CreditInfoDto> updateCredit (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long creditId,
            @RequestBody CreditCreateUpdateDto creditCreateUpdateDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.updateCredit(userId, accountId, creditId, creditCreateUpdateDto));
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<CreditInfoDto>> getCreditByAccountId (
            @PathVariable Long userId,
            @PathVariable Long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.findCreditByAccountId(userId, accountId));
    }

    @GetMapping("/all/status/{statusId}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<CreditInfoDto>> getCreditByAccountIdAndStatusId (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long statusId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.findCreditByAccountIdAndStatusId(userId, accountId, statusId));
    }

    @GetMapping("/active")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<CreditFullInfoDto> getFullInfoAboutActiveCredit (
            @PathVariable Long userId,
            @PathVariable Long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.getFullInfoAboutActiveCredit(userId, accountId));
    }

    @GetMapping("/paymentSchedule")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<CreditMonthlyInfoDto>> drawUpPaymentSchedule (
            @PathVariable Long userId,
            @PathVariable Long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.drawUpPaymentSchedule(userId, accountId));
    }

    @PatchMapping("/{creditId}/status")
    @PreAuthorize("hasAuthority('MANAGER')")
    public ResponseEntity<CreditInfoDto> changeCreditStatus (
            @PathVariable Long userId,
            @PathVariable Long accountId,
            @PathVariable Long creditId,
            @RequestBody CreditStatusDto creditStatusDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(creditFacade.updateCreditStatus(userId, accountId, creditId, creditStatusDto));
    }
}
