package ru.ccfit.inechakhin.web.dto.user;

import java.time.LocalDate;
import java.util.Objects;

public class UserCreateUpdateDto {
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String email;
    private String phoneNumber;
    private UserCredentialDto userCredentialDto;

    public UserCreateUpdateDto () {
    }

    public UserCreateUpdateDto (String firstName, String lastName, LocalDate birthDate, String email, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName () {
        return firstName;
    }

    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    public String getLastName () {
        return lastName;
    }

    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate () {
        return birthDate;
    }

    public void setBirthDate (LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public String getPhoneNumber () {
        return phoneNumber;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserCredentialDto getUserCredentialDto () {
        return userCredentialDto;
    }

    public void setUserCredentialDto (UserCredentialDto userCredentialDto) {
        this.userCredentialDto = userCredentialDto;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCreateUpdateDto that = (UserCreateUpdateDto) o;
        return Objects.equals(firstName, that.firstName)
                && Objects.equals(lastName, that.lastName)
                && Objects.equals(birthDate, that.birthDate)
                && Objects.equals(email, that.email)
                && Objects.equals(phoneNumber, that.phoneNumber);
    }

    @Override
    public int hashCode () {
        return Objects.hash(firstName, lastName, birthDate, email, phoneNumber);
    }

    public static class UserCredentialDto {
        private String username;
        private String password;

        public String getUsername () {
            return username;
        }

        public void setUsername (String username) {
            this.username = username;
        }

        public String getPassword () {
            return password;
        }

        public void setPassword (String password) {
            this.password = password;
        }
    }
}
