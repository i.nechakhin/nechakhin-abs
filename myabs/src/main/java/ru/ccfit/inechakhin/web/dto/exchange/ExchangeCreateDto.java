package ru.ccfit.inechakhin.web.dto.exchange;

import java.math.BigDecimal;

public class ExchangeCreateDto {
    private Long accountFrom;
    private Long accountTo;
    private BigDecimal amount;

    public ExchangeCreateDto () {
    }

    public Long getAccountFrom () {
        return accountFrom;
    }

    public void setAccountFrom (Long accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Long getAccountTo () {
        return accountTo;
    }

    public void setAccountTo (Long accountTo) {
        this.accountTo = accountTo;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }
}
