package ru.ccfit.inechakhin.web.dto.credit;

import ru.ccfit.inechakhin.web.dto.transaction.TransactionSourceDto;

import java.math.BigDecimal;
import java.util.List;

public class CreditFullInfoDto {
    private BigDecimal remainingAmount;
    private BigDecimal remainingTerm;
    private BigDecimal annualRate;
    private BigDecimal paymentForRepaymentCredit;
    private List<TransactionSourceDto> allSource;
    private BigDecimal dailyFine;

    public CreditFullInfoDto () {
    }

    public CreditFullInfoDto (BigDecimal remainingAmount, BigDecimal remainingTerm, BigDecimal annualRate,
                              BigDecimal paymentForRepaymentCredit, List<TransactionSourceDto> allSource, BigDecimal dailyFine) {
        this.remainingAmount = remainingAmount;
        this.remainingTerm = remainingTerm;
        this.annualRate = annualRate;
        this.paymentForRepaymentCredit = paymentForRepaymentCredit;
        this.allSource = allSource;
        this.dailyFine = dailyFine;
    }

    public BigDecimal getRemainingAmount () {
        return remainingAmount;
    }

    public void setRemainingAmount (BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public BigDecimal getRemainingTerm () {
        return remainingTerm;
    }

    public void setRemainingTerm (BigDecimal remainingTerm) {
        this.remainingTerm = remainingTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    public BigDecimal getPaymentForRepaymentCredit () {
        return paymentForRepaymentCredit;
    }

    public void setPaymentForRepaymentCredit (BigDecimal paymentForRepaymentCredit) {
        this.paymentForRepaymentCredit = paymentForRepaymentCredit;
    }

    public List<TransactionSourceDto> getAllSource () {
        return allSource;
    }

    public void setAllSource (List<TransactionSourceDto> allSource) {
        this.allSource = allSource;
    }

    public BigDecimal getDailyFine () {
        return dailyFine;
    }

    public void setDailyFine (BigDecimal dailyFine) {
        this.dailyFine = dailyFine;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditFullInfoDto that = (CreditFullInfoDto) o;
        if (!remainingAmount.equals(that.remainingAmount)) return false;
        if (!remainingTerm.equals(that.remainingTerm)) return false;
        if (!annualRate.equals(that.annualRate)) return false;
        if (!paymentForRepaymentCredit.equals(that.paymentForRepaymentCredit)) return false;
        if (!allSource.equals(that.allSource)) return false;
        return dailyFine.equals(that.dailyFine);
    }

    @Override
    public int hashCode () {
        int result = remainingAmount.hashCode();
        result = 31 * result + remainingTerm.hashCode();
        result = 31 * result + annualRate.hashCode();
        result = 31 * result + paymentForRepaymentCredit.hashCode();
        result = 31 * result + allSource.hashCode();
        result = 31 * result + dailyFine.hashCode();
        return result;
    }

    @Override
    public String toString () {
        return "CreditFullInfoDto{" +
                "remainingAmount=" + remainingAmount +
                ", remainingTerm=" + remainingTerm +
                ", annualRate=" + annualRate +
                ", paymentForRepaymentCredit=" + paymentForRepaymentCredit +
                ", allSource=" + allSource +
                ", dailyFine=" + dailyFine +
                '}';
    }
}
