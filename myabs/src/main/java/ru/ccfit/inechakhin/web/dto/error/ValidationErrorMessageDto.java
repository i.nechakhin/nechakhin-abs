package ru.ccfit.inechakhin.web.dto.error;

import java.util.List;

public class ValidationErrorMessageDto {
    private String message;
    private List<ErrorDescription> errors;

    public ValidationErrorMessageDto () {
    }

    public ValidationErrorMessageDto (String message, List<ErrorDescription> errors) {
        this.message = message;
        this.errors = errors;
    }

    public String getMessage () {
        return message;
    }

    public void setMessage (String message) {
        this.message = message;
    }

    public List<ErrorDescription> getErrors () {
        return errors;
    }

    public void setErrors (List<ErrorDescription> errors) {
        this.errors = errors;
    }

    public static class ErrorDescription {
        private String field;
        private String error;

        public ErrorDescription () {
        }

        public ErrorDescription (String field, String error) {
            this.field = field;
            this.error = error;
        }

        public String getField () {
            return field;
        }

        public void setField (String field) {
            this.field = field;
        }

        public String getError () {
            return error;
        }

        public void setError (String error) {
            this.error = error;
        }
    }
}
