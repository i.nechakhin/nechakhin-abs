package ru.ccfit.inechakhin.web.dto.user;

public class UserStatusDto {
    private String status;

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }
}
