package ru.ccfit.inechakhin.web.dto.transaction;

public class TransactionVerificationCodeDto {
    private Integer verificationCode;

    public TransactionVerificationCodeDto () {
    }

    public TransactionVerificationCodeDto (Integer verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Integer getVerificationCode () {
        return verificationCode;
    }

    public void setVerificationCode (Integer verificationCode) {
        this.verificationCode = verificationCode;
    }
}
