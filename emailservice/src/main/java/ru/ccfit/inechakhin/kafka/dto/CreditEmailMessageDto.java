package ru.ccfit.inechakhin.kafka.dto;

import java.math.BigDecimal;

public class CreditEmailMessageDto extends EmailMessageDto {
    private BigDecimal remainingAmount;
    private BigDecimal remainingTerm;
    private BigDecimal annualRate;
    private String status;

    public CreditEmailMessageDto () {
    }

    public CreditEmailMessageDto (BigDecimal remainingAmount, BigDecimal remainingTerm, BigDecimal annualRate, String status) {
        this.remainingAmount = remainingAmount;
        this.remainingTerm = remainingTerm;
        this.annualRate = annualRate;
        this.status = status;
    }

    public BigDecimal getRemainingAmount () {
        return remainingAmount;
    }

    public void setRemainingAmount (BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public BigDecimal getRemainingTerm () {
        return remainingTerm;
    }

    public void setRemainingTerm (BigDecimal remainingTerm) {
        this.remainingTerm = remainingTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }
}
