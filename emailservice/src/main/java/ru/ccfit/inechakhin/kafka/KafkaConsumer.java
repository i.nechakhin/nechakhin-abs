package ru.ccfit.inechakhin.kafka;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.ccfit.inechakhin.kafka.dto.*;
import ru.ccfit.inechakhin.service.EmailSenderService;

import java.util.function.Consumer;

@Component
public class KafkaConsumer {
    private final EmailSenderService emailSenderService;

    public KafkaConsumer (EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @Bean
    public Consumer<AccountEmailMessageDto> accountEventEmailMessage () {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<BlockingEmailMessageDto> blockingEventEmailMessage () {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<CardEmailMessageDto> cardEventEmailMessage () {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<CreditEmailMessageDto> creditEventEmailMessage () {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<RatesEmailMessageDto> ratesEventEmailMessage () {
        return emailSenderService::sendEventEmail;
    }

    @Bean
    public Consumer<UserEmailMessageDto> userEventEmailMessage () {
        return emailSenderService::sendEventEmail;
    }
}
