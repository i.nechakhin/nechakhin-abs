package ru.ccfit.inechakhin.kafka.dto;

import java.time.LocalDateTime;

public class BlockingEmailMessageDto extends EmailMessageDto {
    private LocalDateTime startBlock;
    private LocalDateTime endBlock;

    public BlockingEmailMessageDto () {
    }

    public BlockingEmailMessageDto (LocalDateTime startBlock, LocalDateTime endBlock) {
        this.startBlock = startBlock;
        this.endBlock = endBlock;
    }

    public LocalDateTime getStartBlock () {
        return startBlock;
    }

    public void setStartBlock (LocalDateTime startBlock) {
        this.startBlock = startBlock;
    }

    public LocalDateTime getEndBlock () {
        return endBlock;
    }

    public void setEndBlock (LocalDateTime endBlock) {
        this.endBlock = endBlock;
    }
}
