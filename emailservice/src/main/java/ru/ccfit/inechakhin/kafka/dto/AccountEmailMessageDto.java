package ru.ccfit.inechakhin.kafka.dto;

import java.time.LocalDateTime;

public class AccountEmailMessageDto extends EmailMessageDto {
    private LocalDateTime updatedAt;
    private String status;

    public AccountEmailMessageDto () {
    }

    public AccountEmailMessageDto (LocalDateTime updatedAt, String currency, String status) {
        this.updatedAt = updatedAt;
        this.status = status;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }
}
