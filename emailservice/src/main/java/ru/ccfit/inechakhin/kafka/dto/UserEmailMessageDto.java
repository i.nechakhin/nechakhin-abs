package ru.ccfit.inechakhin.kafka.dto;

import java.time.LocalDateTime;

public class UserEmailMessageDto extends EmailMessageDto {
    private String firstName;
    private String phoneNumber;
    private LocalDateTime updatedAt;
    private String status;

    public UserEmailMessageDto () {
    }

    public UserEmailMessageDto (String firstName, String phoneNumber, LocalDateTime updatedAt, String status) {
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.updatedAt = updatedAt;
        this.status = status;
    }

    public String getFirstName () {
        return firstName;
    }

    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber () {
        return phoneNumber;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }
}
