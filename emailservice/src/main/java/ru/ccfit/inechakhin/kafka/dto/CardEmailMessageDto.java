package ru.ccfit.inechakhin.kafka.dto;

import java.time.LocalDate;

public class CardEmailMessageDto extends EmailMessageDto {
    private Long number;
    private LocalDate expDate;
    private String status;
    private String paymentSystem;

    public CardEmailMessageDto () {
    }

    public CardEmailMessageDto (Long number, LocalDate expDate, String status, String paymentSystem) {
        this.number = number;
        this.expDate = expDate;
        this.status = status;
        this.paymentSystem = paymentSystem;
    }

    public Long getNumber () {
        return number;
    }

    public void setNumber (Long number) {
        this.number = number;
    }

    public LocalDate getExpDate () {
        return expDate;
    }

    public void setExpDate (LocalDate expDate) {
        this.expDate = expDate;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getPaymentSystem () {
        return paymentSystem;
    }

    public void setPaymentSystem (String paymentSystem) {
        this.paymentSystem = paymentSystem;
    }
}
