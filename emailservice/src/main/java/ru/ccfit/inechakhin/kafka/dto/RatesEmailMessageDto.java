package ru.ccfit.inechakhin.kafka.dto;

import java.math.BigDecimal;

public class RatesEmailMessageDto extends EmailMessageDto {
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private BigDecimal minTerm;
    private BigDecimal maxTerm;
    private BigDecimal annualRate;

    public RatesEmailMessageDto () {
    }

    public RatesEmailMessageDto (BigDecimal minAmount, BigDecimal maxAmount, BigDecimal minTerm, BigDecimal maxTerm, BigDecimal annualRate) {
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.minTerm = minTerm;
        this.maxTerm = maxTerm;
        this.annualRate = annualRate;
    }

    public BigDecimal getMinAmount () {
        return minAmount;
    }

    public void setMinAmount (BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount () {
        return maxAmount;
    }

    public void setMaxAmount (BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public BigDecimal getMinTerm () {
        return minTerm;
    }

    public void setMinTerm (BigDecimal minTerm) {
        this.minTerm = minTerm;
    }

    public BigDecimal getMaxTerm () {
        return maxTerm;
    }

    public void setMaxTerm (BigDecimal maxTerm) {
        this.maxTerm = maxTerm;
    }

    public BigDecimal getAnnualRate () {
        return annualRate;
    }

    public void setAnnualRate (BigDecimal annualRate) {
        this.annualRate = annualRate;
    }
}
