package ru.ccfit.inechakhin.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.ccfit.inechakhin.kafka.dto.*;

@Service
public class EmailSenderService {
    @Value("${my.mail.username}")
    private static final String MESSAGE_SET_FROM = "ilya-n18@yandex.ru";

    private static final String MESSAGE_SUBJECT = "Your %s was %s";

    private static final String ACCOUNT_MESSAGE_BODY =
            "Your %s was %s at %s. Actual status is %s";

    private static final String BLOCKING_MESSAGE_BODY =
            "Your %s was %s. Actual start block is %s and end block is %s";

    private static final String CARD_MESSAGE_BODY =
            "Your %s number %s was %s. Actual payment system is %s, status is %s, and expiration date is %s";

    private static final String CREDIT_MESSAGE_BODY =
            "Your %s was %s. Actual remaining amount is %s, remaining term is %s, annual rate is %s and status is %s";

    private static final String RATES_MESSAGE_BODY =
            "Your %s was %s. Actual min amount is %s, max amount is %s, min term is %s, max term is %s and annual rate is %s";

    private static final String USER_MESSAGE_BODY =
            "Dear %s, your %s was %s at %s. Actual phone number is %s and status is %s";

    private static final String ACCOUNT = "account";

    private static final String BLOCKING = "blocking";

    private static final String CARD = "card";

    private static final String CREDIT = "credit";

    private static final String RATES = "rates";

    private static final String USER = "profile";

    private final JavaMailSender mailSender;

    public EmailSenderService (JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendSimpleEmail (String toEmail, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(MESSAGE_SET_FROM);
        message.setTo(toEmail);
        message.setSubject(subject);
        message.setText(body);

        mailSender.send(message);
    }

    public void sendEventEmail (AccountEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, ACCOUNT, emailMessageDto.getEvent()),
                String.format(
                        ACCOUNT_MESSAGE_BODY,
                        ACCOUNT,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getUpdatedAt(),
                        emailMessageDto.getStatus()
                )
        );
    }

    public void sendEventEmail (BlockingEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, ACCOUNT, emailMessageDto.getEvent()),
                String.format(
                        BLOCKING_MESSAGE_BODY,
                        BLOCKING,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getStartBlock(),
                        emailMessageDto.getEndBlock()
                )
        );
    }

    public void sendEventEmail (CardEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, CARD, emailMessageDto.getEvent()),
                String.format(
                        CARD_MESSAGE_BODY,
                        CARD,
                        emailMessageDto.getNumber(),
                        emailMessageDto.getEvent(),
                        emailMessageDto.getPaymentSystem(),
                        emailMessageDto.getStatus(),
                        emailMessageDto.getExpDate()
                )
        );
    }

    public void sendEventEmail (CreditEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, ACCOUNT, emailMessageDto.getEvent()),
                String.format(
                        CREDIT_MESSAGE_BODY,
                        CREDIT,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getRemainingAmount(),
                        emailMessageDto.getRemainingTerm(),
                        emailMessageDto.getAnnualRate(),
                        emailMessageDto.getStatus()
                )
        );
    }

    public void sendEventEmail (RatesEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, ACCOUNT, emailMessageDto.getEvent()),
                String.format(
                        RATES_MESSAGE_BODY,
                        RATES,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getMinAmount(),
                        emailMessageDto.getMaxAmount(),
                        emailMessageDto.getMinTerm(),
                        emailMessageDto.getMaxTerm(),
                        emailMessageDto.getAnnualRate()
                )
        );
    }

    public void sendEventEmail (UserEmailMessageDto emailMessageDto) {
        sendSimpleEmail(
                emailMessageDto.getUserEmail(),
                String.format(MESSAGE_SUBJECT, USER, emailMessageDto.getEvent()),
                String.format(
                        USER_MESSAGE_BODY,
                        emailMessageDto.getFirstName(),
                        USER,
                        emailMessageDto.getEvent(),
                        emailMessageDto.getUpdatedAt(),
                        emailMessageDto.getPhoneNumber(),
                        emailMessageDto.getStatus()
                )
        );
    }
}
