package ru.ccfit.inechakhin.kafka.dto;

import java.math.BigDecimal;

public class TransactionCodeDto {
    private int code;
    private BigDecimal amount;
    private Long chatId;

    public TransactionCodeDto () {
    }

    public TransactionCodeDto (int code, BigDecimal amount, Long chatId) {
        this.code = code;
        this.amount = amount;
        this.chatId = chatId;
    }

    public int getCode () {
        return code;
    }

    public void setCode (int code) {
        this.code = code;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public Long getChatId () {
        return chatId;
    }

    public void setChatId (Long chatId) {
        this.chatId = chatId;
    }
}
