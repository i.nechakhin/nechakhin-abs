package ru.ccfit.inechakhin.kafka;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.ccfit.inechakhin.kafka.dto.*;
import ru.ccfit.inechakhin.util.MessageGenerator;
import ru.ccfit.inechakhin.util.TelegramUpdateProcessor;

import java.util.function.Consumer;

@Component
public class KafkaConsumer {
    private final TelegramUpdateProcessor telegramUpdateProcessor;

    private final static String TRANSACTION_CODE_MESSAGE = """
            Код подтверждения операции: %d \n
            Операция на сумму: %s \n
            Если вы не совершали ее, обратитеть в служюу поддержки банка
            """;

    public KafkaConsumer (TelegramUpdateProcessor telegramUpdateProcessor) {
        this.telegramUpdateProcessor = telegramUpdateProcessor;
    }

    @Bean
    public Consumer<TelegramCheckTokenResultDto> tokenVerificationResponse () {
        return telegramCheckTokenResultDto -> {
            Long chatId = telegramCheckTokenResultDto.getChatId();
            SendMessage message;
            if (telegramCheckTokenResultDto.isTokenVerified()) {
                TelegramUpdateProcessor.SCENERY_MAP.put(chatId, TelegramUpdateProcessor.ALREADY_REGISTERED);
                message = MessageGenerator.generateSendMessage("Вы успешно прошли проверку", chatId);
            } else {
                message = MessageGenerator.generateSendMessage("Токен неверный, попробуйте еще раз", chatId);
            }
            telegramUpdateProcessor.send(message);
        };
    }

    @Bean
    public Consumer<TransactionCodeDto> transactionCode () {
        return transactionCodeDto -> {
            SendMessage message = MessageGenerator.generateSendMessage(
                    String.format(
                            TRANSACTION_CODE_MESSAGE,
                            transactionCodeDto.getCode(),
                            transactionCodeDto.getAmount()
                    ),
                    transactionCodeDto.getChatId()
            );
            telegramUpdateProcessor.send(message);
        };
    }
}