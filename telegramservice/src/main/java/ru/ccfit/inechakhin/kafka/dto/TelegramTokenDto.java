package ru.ccfit.inechakhin.kafka.dto;

public class TelegramTokenDto {
    private String token;
    private Long chatId;

    public TelegramTokenDto () {
    }

    public TelegramTokenDto (String token, Long chatId) {
        this.token = token;
        this.chatId = chatId;
    }

    public String getToken () {
        return token;
    }

    public void setToken (String token) {
        this.token = token;
    }

    public Long getChatId () {
        return chatId;
    }

    public void setChatId (Long chatId) {
        this.chatId = chatId;
    }

    @Override
    public String toString () {
        return "TelegramTokenDto{" +
                "token='" + token + '\'' +
                ", chatId=" + chatId +
                '}';
    }
}
