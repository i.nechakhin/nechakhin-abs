package ru.ccfit.inechakhin.kafka;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;
import ru.ccfit.inechakhin.kafka.dto.TelegramTokenDto;

@Component
public class KafkaProducer {
    private final StreamBridge streamBridge;

    private static final String BINDING_NAME = "tokenVerificationRequest-out-0";

    public KafkaProducer (StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    public void tokenVerificationRequest (TelegramTokenDto telegramTokenDto) {
        streamBridge.send(BINDING_NAME, telegramTokenDto);
    }
}