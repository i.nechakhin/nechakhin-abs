package ru.ccfit.inechakhin.kafka.dto;

public class TelegramCheckTokenResultDto {
    private Long chatId;
    private boolean isTokenVerified;

    public TelegramCheckTokenResultDto () {
    }

    public TelegramCheckTokenResultDto (Long chatId, boolean isTokenVerified) {
        this.chatId = chatId;
        this.isTokenVerified = isTokenVerified;
    }

    public Long getChatId () {
        return chatId;
    }

    public void setChatId (Long chatId) {
        this.chatId = chatId;
    }

    public boolean isTokenVerified () {
        return isTokenVerified;
    }

    public void setTokenVerified (boolean tokenVerified) {
        this.isTokenVerified = tokenVerified;
    }
}
