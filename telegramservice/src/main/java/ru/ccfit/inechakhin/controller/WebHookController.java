package ru.ccfit.inechakhin.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.ccfit.inechakhin.util.TelegramUpdateProcessor;

@RestController
public class WebHookController {
    private final TelegramUpdateProcessor updateProcessor;

    public WebHookController (TelegramUpdateProcessor updateProcessor) {
        this.updateProcessor = updateProcessor;
    }

    @PostMapping(value = "/callback/update")
    public ResponseEntity<?> onUpdateReceived (@RequestBody Update update) {
        updateProcessor.processUpdate(update);
        return ResponseEntity.ok().build();
    }
}