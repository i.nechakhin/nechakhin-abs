package ru.ccfit.inechakhin.config;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class TelegramBot extends TelegramWebhookBot {
    @Value("${bot.name}")
    private String botName;

    @Value("${bot.uri}")
    private String botUri;

    public TelegramBot (@Value("${bot.token}") String botToken) {
        super(botToken);
    }

    @PostConstruct
    public void init () {
        try {
            SetWebhook setWebhook = SetWebhook.builder().url(botUri).build();
            this.setWebhook(setWebhook);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendAnswerMessage (SendMessage message) {
        if (message != null) {
            try {
                execute(message);
            } catch (TelegramApiException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived (Update update) {
        return null;
    }

    @Override
    public String getBotPath () {
        return "/update";
    }

    @Override
    public String getBotUsername () {
        return botName;
    }
}